#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
archive_evt.py takes as parameter the rst file of an event where the schedule
directive is used for the event's program and replace the directive with
program's content in rst. Additionnaly, the number of participants is added
in the event's description, just before the program.

Usage:
python3 archive_evt.py path/to/evt_to_archive.rst nb_participants
"""

import sys
import os
from pathlib import Path
import re
try:
    from plugins import event_schedule
except ImportError:
    head = os.getcwd()
    head, tail = os.path.split(head)
    while tail != 'website' and head != '/' and head != '':
        head, tail = os.path.split(head)

    print("You have to set up properly your PYTHONPATH:")
    if tail == "website":
        print("export PYTHONPATH=$PYTHONPATH:%s/website/plugins" % (head))
    else:
        print("export PYTHONPATH=$PYTHONPATH:/path_to_website_repo/plugins")
    sys.exit(1)


def fetch_line_schedule(evt_content):
    schedule_pattern = re.compile(r"^([\s]*)\.\. schedule::")

    for i, l in enumerate(evt_content):
        res = schedule_pattern.search(l)
        if res:
            return i, len(res.group(1))

    return -1, None


def fetch_indico_id(evt_content, l_sched):
    indico_url_pattern = re.compile(r"[\s]*:indico_url: ([\W\w0-9/:\.]*)\n")
    indico_event_pattern = re.compile(r"[\s]*:indico_event: ([0-9]*)")

    indico_url = ""
    indico_event = -1
    item = l_sched+1
    while (indico_url == "" or indico_event == -1) and item < len(evt_content):
        res = indico_url_pattern.search(evt_content[item])
        if res:
            indico_url = res.group(1)

        res = indico_event_pattern.search(evt_content[item])
        if res:
            indico_event = int(res.group(1))

        item += 1

    return indico_url, indico_event, item


def indent_content(content, indent):
    to_return = ['\n']
    for line in content.splitlines():
        to_return.append(" "*indent + line + '\n')

    return to_return


def add_nb_participants(evt_content, nb_participants):
    looking_for = ".. section:: Programme\n"
    try:
        line = evt_content.index(looking_for)
    except:
        print("Error: event does not contain the directive: %s" % looking_for)
        sys.exit(1)

    to_be_inserted = ["    Nombre de participants : %d\n" %
                      nb_participants, "\n"]
    return evt_content[:line] + to_be_inserted + evt_content[line:]


def gen_rst_evt_with_program(evt_content, nb_participants):
    evt_content = add_nb_participants(evt_content, nb_participants)

    l_sched, content_indent = fetch_line_schedule(evt_content)
    if l_sched < 0:
        print("Error: no schedule directive in the file")
        sys.exit(1)

    url, evt, last_line = fetch_indico_id(evt_content, l_sched)
    if url == "" or evt < 0:
        print("Error: could not find indico_url or indico_event directives")
        sys.exit(1)

    prog_to_insert_str = event_schedule.indico_event_to_rst(url, evt)
    prog_to_insert = indent_content(prog_to_insert_str, content_indent+4)

    return evt_content[:l_sched+1] + prog_to_insert + evt_content[last_line:]


def build_support_dir(evt_orig):
    evt_name, rst_ext = os.path.splitext(evt_orig)
    evt_name = os.path.basename(evt_name)
    head = os.path.abspath(evt_orig)
    head, tail = os.path.split(head)
    while tail != 'website' and head != '/' and head != '':
        head, tail = os.path.split(head)
    if tail != "website":
        print("Error: Please convert event from the website repository")
        sys.exit(1)

    return os.path.join(head, 'website', 'content', 'attachments', 'evt',
                        evt_name)


def truncate(dest_dir):
    head, tail = os.path.split(dest_dir)
    truncated = tail
    while tail != 'content' and head != '/' and head != '':
        head, tail = os.path.split(head)
        if tail != "content":
            truncated = os.path.join(tail, truncated)
    if tail != "content":
        print("Error: Destination directory for supports must be in the "
              "content directory")
        sys.exit(1)

    return truncated


def online_pres(url_support):
    # Considered as an online presentation if not an archive nor a pdf
    ext = os.path.splitext(url_support)[-1]
    return ext not in (".tgz", ".gz", ".bz2", ".zip", ".pdf", ".org")


def treat_support(line_orig, url_support, dest_dir, support_id):
    import urllib.request
    new_line = ""
    new_support = None
    if online_pres(url_support):
        new_line = line_orig.split('(')[0] + '(' + url_support + ')\n'
    else:
        try:
            response = urllib.request.urlopen(url_support)
        except urllib.error.URLError:
            # Handle an error that may occur on MacOS
            import ssl
            ssl._create_default_https_context = ssl._create_unverified_context
            response = urllib.request.urlopen(url_support)
        content = response.read()
        root, ext = os.path.splitext(url_support)
        new_support = os.path.join(
            dest_dir, 'support%02d%s' % (support_id, ext))
        f = open(new_support, "wb")
        f.write(content)
        f.close()
        rst_support = os.path.join(
            truncate(dest_dir), 'support%02d%s' % (support_id, ext))
        new_line = line_orig.split('(')[0] + '(' + rst_support + ')\n'
    return new_line, new_support


def fetch_supports(content_with_prog, evt_orig):
    dest_dir = build_support_dir(evt_orig)
    try:
        Path(dest_dir).mkdir(exist_ok=True)
    except FileExistsError:
        print("Error: Directory %s already exists" % dest_dir)
        sys.exit(1)

    git_helper_str = "git add \\\n%s \\\n" % evt_orig
    support_url_pattern = re.compile(
        r"[\s]*\[support [0-9]*\]\(([\W\w0-9/:\.-_]*)\)")
    support_id = 0
    for i, l in enumerate(content_with_prog):
        res = support_url_pattern.search(l)
        if res is not None:
            print("Treating %s" % res.group(1))
            new_line, new_support = treat_support(
                l, res.group(1), dest_dir, support_id)
            content_with_prog[i] = new_line
            if new_support:
                git_helper_str += "%s \\\n" % new_support
                support_id += 1

    return git_helper_str, content_with_prog


def fetch_content(evt_orig):
    f = open(evt_orig, "r")
    c = f.readlines()
    f.close()
    return c


def write_content(content_with_prog, evt_to_treat):
    f = open(evt_to_treat, "w")
    f.write("".join(content_with_prog))
    f.close()


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("""\
archive_evt.py takes as parameter the rst file of an event
where the schedule directive is used for the event's program
and replace the directive with program's content in rst.

Usage:
python3 %s path/to/evt_to_archive.rst nb_participants""" % (sys.argv[0]))
        sys.exit(1)

    evt_to_treat = sys.argv[1]
    nb_participants = int(sys.argv[2])
    evt_content = fetch_content(evt_to_treat)
    content_with_prog = gen_rst_evt_with_program(evt_content, nb_participants)
    user_msg, prog_to_insert = fetch_supports(content_with_prog, evt_to_treat)
    # write_content(content_with_prog, "pok")
    write_content(content_with_prog, evt_to_treat)
    print("\nrst file modified and supports downloaded. "
          "Please add them to the repository.\n\n%s" % user_msg)

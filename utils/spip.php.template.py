#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Permanently redirect urls from old web site to the new one.

If a requested url is not in the url matches dictionary,
then the client is redirected to the home page.
"""

# Old -> new urls links
url_links = {
        {% for spip_name, pelican_name in matches %}'{{spip_name}}': '{{pelican_name}}',
    {% endfor %}
}


def get_new_url(page_id):
    """ Return new url given a spip page id. Home page if unknown. """
    try:
        return url_links[page_id] + '.html'
    except:
        return ''


###############################################################################
if __name__ == '__main__':

    import os

    # See https://stackoverflow.com/questions/11842547/distinguish-from-command-line-and-cgi-in-python
    if 'GATEWAY_INTERFACE' in os.environ: # CGI interface
        import cgi

        try:
            page_id = cgi.FieldStorage(keep_blank_values=True).keys()[0]
        except:
            page_id = ''

    else: # Command-line interface
        import sys

        try:
            page_id = sys.argv[1]
        except:
            page_id = ''


    # HTTP header
    print('Status: 301 Moved Permanently');
    print('Location: ./{}'.format(get_new_url(page_id)));
    print('')


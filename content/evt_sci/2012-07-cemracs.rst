CEMRACS 2012
############

:date: 2012-10-05 07:40:45
:modified: 2012-10-05 07:40:45
:category: formation
:tags: cemracs
:start_date: 2012-07-16
:end_date: 2012-08-24
:place: Luminy
:summary: Le CEMRACS (Centre d'Eté Mathématique de Recherche Avancée en Calcul Scientifique) est une école d'été de 6 semaines. Il a été organisé en 2012 par le Groupe Calcul.


.. contents::

.. section:: Description
    :class: description


    Le `CEMRACS <http://smai.emath.fr/spip.php?article51>`__ (Centre d'Eté Mathématique de Recherche Avancée en Calcul Scientifique) est une école d'été qui existe depuis 1996. Cet évènement sera organisé en 2012 par le GDR Calcul.

    Le CEMRACS 2012 aura pour thème **"méthodes numériques et algorithmes pour architectures pétaflopiques"**  et permettra d'aborder les avancées scientifiques récentes dans ces domaines.

    L'école se déroule en deux phases, la première consiste en une semaine de cours et la seconde en une session de recherche de 5 semaines. Cette école a lieu au CIRM à Luminy (Marseille). La version 2012 se tiendra du 16 juillet au 24 août 2012.

    Lors de la deuxième phase, chaque participant travaille en équipe sur un projet proposé soit par un industriel soit par une équipe universitaire. Les équipes sont composées de jeunes chercheurs encadrés par un ou deux chercheurs confirmés. Un séminaire quotidien est organisé afin de susciter des échanges entre les participants et de permettre l'acquisition de nouvelles connaissances dans le thème considéré.

    Le `site officiel <http://smai.emath.fr/cemracs/cemracs12/>`__ du CEMRACS 2012.


.. section:: École d'été
    :class: programme

    .. schedule::

        .. day:: 16-07-2012

            .. event:: Extrapolation and Krylov Subspaces Methods for Solving Linear Equations
                :begin: 09:30
                :end: 12:30
                :speaker: Martin Gander, Université de Genève
                :support: attachments/spip/Documents/Ecoles/CEMRACS2012/MGander.pdf

            .. event:: Massively Parallel Splitting Algorithms for the Incompressible and Slightly Compressible Navier-Stokes Equations
                :begin: 14:00
                :end: 17:30
                :speaker: Jean-Luc Guermond, Texas A&M University
                :support:
                    [Partie 1](attachments/spip/Documents/Ecoles/CEMRACS2012/fspm_marseille_Guermond1.pdf)
                    [Partie 2](attachments/spip/Documents/Ecoles/CEMRACS2012/var_den_marseille_Guermond2.pdf)
                    [Partie 3](attachments/spip/Documents/Ecoles/CEMRACS2012/entrop_visc_HYPER_2012_Guermond3.pdf)


        .. day:: 17-07-2012

            .. event:: Parallel Multigrid Methods, Simulating Complex Flows with the Lattice Boltzmann Method
                :begin: 09:30
                :end: 12:30
                :speaker: Ulrich Rüde, University Erlangen-Nuremberg
                :support:
                    [Partie 1](attachments/spip/Documents/Ecoles/CEMRACS2012/lbm-v1-cpr_Rude1.pdf)
                    [Partie 2](attachments/spip/Documents/Ecoles/CEMRACS2012/parallel-multigrid-v1-cpr_Rude2.pdf)

            .. event:: Quantification of Uncertainties in High-Fidelity Simulations of Turbulent Reactive Flows
                :begin: 14:00
                :end: 17:30
                :speaker: Gianluca Iaccarino, Standfort University
                :support:
                    [Partie 1](attachments/spip/Documents/Ecoles/CEMRACS2012/iaccarino-shortcourse-uq-part1.pdf)
                    [Partie 2](attachments/spip/Documents/Ecoles/CEMRACS2012/iaccarino-shortcourse-uq-part2.pdf)
                    [Partie 3](attachments/spip/Documents/Ecoles/CEMRACS2012/iaccarino-shortcourse-uq-part3.pdf)
                    [Partie 4](attachments/spip/Documents/Ecoles/CEMRACS2012/iaccarino-shortcourse-uq-part4.pdf)


        .. day:: 18-07-2012

            .. event:: Aggregation-Based Algebraic Multigrid : from Theory to Fast Solvers
                :begin: 09:30
                :end: 10:30
                :speaker: Yvan Notay, Université Libre de Bruxelles
                :support: attachments/spip/Documents/Ecoles/CEMRACS2012/marseille12_notay.pdf

            .. event:: Particles Methods
                :begin: 11:00
                :end: 17:30
                :speaker: Petros Koumoutsakos, ETH Zürich
                :support:
                    [Partie 1](attachments/spip/Documents/Ecoles/CEMRACS2012/cemracs_koumoutsakos_1.pdf)
                    [Partie 2](attachments/spip/Documents/Ecoles/CEMRACS2012/cemracs_koumoutsakos_2.pdf)
                    [Partie 3](attachments/spip/Documents/Ecoles/CEMRACS2012/cemracs_koumoutsakos_3.pdf)

                - Unbounded Domains and Multiresolution
                - Boundary Conditions and Multi-Scaling


        .. day:: 19-07-2012

            .. event:: Two-Level Domain Decomposition Methods
                :begin: 09:30
                :end: 12:30
                :speaker: Frédéric Nataf, Université Paris 6
                :support:
                    [Partie 1](attachments/spip/Documents/Ecoles/CEMRACS2012/cours-schwarzCemracs2012_Nataf2.pdf)
                    [Partie 2](attachments/spip/Documents/Ecoles/CEMRACS2012/P_JolivetDD21_Nataf1.pdf)
                    [Partie 3](attachments/spip/Documents/Ecoles/CEMRACS2012/talkFredericNataf3.pdf)
                    [Exemples FreeFem++](attachments/spip/Documents/Ecoles/CEMRACS2012/FreefemProgram_Nataf.zip)


            .. event:: Residual Distribution: Basics, Recent Developments, and Relations with others techniques
                :begin: 14:00
                :end: 17:30
                :speaker: Mario Ricchiuto, INRIA Bordeaux - Sud-Ouest
                :support:
                    [Partie 1](attachments/spip/Documents/Ecoles/CEMRACS2012/cemracs2012-p1_ricchiuto.pdf)
                    [Partie 2](attachments/spip/Documents/Ecoles/CEMRACS2012/cemracs2012-p2_ricchiuto.pdf)


        .. day:: 20-07-2012

            .. event:: The Discontinuous Galerkin Method: Discretisation, Efficient Implementation and Application to Turbulent Flows
                :begin: 09:30
                :end: 12:30
                :speaker: Koen Hillewaert, CENAERO
                :support:
                    [Partie 1](attachments/spip/Documents/Ecoles/CEMRACS2012/Hillewaerth-1.pdf)
                    [Partie 2](attachments/spip/Documents/Ecoles/CEMRACS2012/Hillewaerth-2.pdf)
                    [Animation 1](attachments/spip/Documents/Ecoles/CEMRACS2012/cenaero_LPTurbine.avi)
                    [Animation 2](attachments/spip/Documents/Ecoles/CEMRACS2012/cenaero_SD7003_transition.avi)


            .. event:: Présentation du mésocentre de Calcul d'Aix-Marseille Université
                :begin: 14:00
                :end: 14:30
                :speaker: Fabien Archambault, Aix-Marseille Université
                :support:
                    [Slides](attachments/spip/Documents/Ecoles/CEMRACS2012/Presentation_CEMRACS_us_meso.pdf)


            .. event:: Présentation des projets de la session de recherche
                :begin: 14:30
                :end: 17:30


.. section:: Projets de recherche
    :class: description

    - `Détails des projets <attachments/spip/Documents/Ecoles/CEMRACS2012/projects_details.pdf>`__
    - `Détails des équipes <attachments/spip/Documents/Ecoles/CEMRACS2012/projects_team.pdf>`__

    .. section:: COLARGOL
        :class: description

        **COmparaison des aLgorithmes dans AeRosol et aGhOra pour les fLuides compressibles.**

        Dans ce projet, nous proposons de comparer les algorithmes de méthodes éléments finis d'ordre élevé implanteés dans deux librairies éléments finis développeés depuis un an: AGHORA, développé à l'ONERA/DSNA, et AEROSOL, développé à INRIA Bordeaux Sud-Ouest (équipes Bacchus et Cagire). Les principales similitudes entre les deux codes sont : - La prise en compte d'un ordre arbitrairement grand et de maillages quelconques. - L'implantation de méthodes de Galerkine discontinu. Les principales différences sont : - des méthodes à distribution de résidus en éléments finis continus sont implantées dans AEROSOL. - Le langage de programmation (Fortran pour AGHORA et C++ pour AEROSOL). Nous proposons dans ce projet de comparer les implantations des opérations élémentaires (locales à une cellule), ainsi que les stratégies en terme de parallélisme et de factorisation de code. Le point de départ de la comparaison se fera à partir de deux cas tests: le vortex isentropique en deux dimensions, ainsi qu'un cas d'écoulement autour d'une bosse régulière.


    .. section:: FullSWOF
        :class: description

        **Parallélisation du logiciel FullSWOF_2D qui résout les équations de Saint-venant, sur une topographique haute résolution.**

        L'objectif de ce projet consiste à développer et tester une version parallèle du logiciel FullSWOF_2D qui résout les équations de Saint-venant, sur une topographique haute résolution, en utilisant une stratégie de décomposition de domaine et en comparant 2 approches líune "classique" basée sur une architecture maître-esclave utilisant MPI et líautre utilisant des algorithmes squelettiques (OSL, Orléans Skeleton Library et SkelGIS Skeletons for Geographical Information Systems). On comparera ces 2 approches en terme de performances, scalabilité.


    .. section:: HAMM
        :class: description

        **Décomposition de domaine, solveur grille grossière, application à la mécanique non-linéaire.**

        Nous avons développé un code permettant de tester différentes méthodes de décomposition de domaines de type Schwartz (DD, DN, NN, RR) sur des problèmes linéaire 2D et 3D. Lors ce cemracs nous proposons díimplémenter dans Feel++ de nouveaux préconditioneurs grilles grossières développés par líéquipe autour de F. Nataf (UPMC/CNRS) qui ont déjà été testés sur FreeFem++. On travaillera également sur un préconditioneur 2D/3D pour la méthode des joints en parallèle en collaboration avec S. Bertoluzza. Enfin nous nous proposons également de mettre en place un benchmark de comparaison de solveurs/préconditionneurs pour des problèmes non-linéaires en mécanique des solides. Ce benchmark sera implémenté à la fois en FreeFem++ et Feel++. Des tests et comparaisons seront effectués sur le TGCC. Les figures ci-contre illustrent un calcul sur 128 processeurs.


    .. section:: HTP
        :class: description

        **High performance solvers for Tokamak Physics**

        Understanding and control of turbulent transport in thermonuclear plasmas in magnetic confinement devices is a major goal. This aspect of first principle physics plays a key role in achieving the level of performance expected in fusion reactors. In the ITER design1, the latter was estimated by extrapolating an empirical law. The simulation and understanding of the turbulent transport in Fusion plasmas remains therefore an ambitious endeavour. The following project aims at building parallel tools in order to understand turbulent transport in thermonuclear plasmas of magnetic Fusion devices. The long-term objective is to analyse present experiments, to predict the performances of next step devices including ITER, and to propose possible routes for control.


    .. section:: Spray on GPU
        :class: description

        **On the development of high order realizable moments methods for the simulation of sprays : adaptation to GPU/hybrid architectures.**

        Parallélisation distribué d'un code volume fini d'écoulement compositionnel polyphasique en milieux poreux, applications aux stockage géologique du CO2 et au stockage des déchets radioactifs.


    .. section:: POAM2-AHyMaHT
        :class: description

        **Parallelization and optimization of adaptive multiresolution methodologies: application to hydrodynamic and magnetohydrodynamic turbulence.**

        The aim of the project is to make further progress on the development and parallelization of adaptive multiresolution methods (MR)for modeling and computing fully developed turbulent flows, for either electrically neutral (hydrodynamic) or electrically conducting (magneto-hydrodynamic) fluids. Different data structures, pointer based octrees (and binary trees) or patch based approaches will be examined. The coherent vorticity simulation approach will be further developed using fully adaptive solvers.


    .. section:: Cloud
        :class: description

        **Quantification de l'élasticité du cloud en fonction de la variation des demandes modélisées comme un flux turbulent**

        Le Cloud peut se résumer comme un modèle de mise à disposition des ressources informatiques (serveurs, réseaux et stockage) à la demande. Ce modèle est censé permettre la diminution des coûts, les ressources étant éteintes en absence de besoin et activées en cas de demande. Cette caractérisque du cloud est communément appelé líélasticité. Il s'agit dans ce projet de modéliser mathématiquement l'élasticité du cloud et de proposer un processus automtisable permettant de déclencher « au meilleur moment » la mise à disposition des ressources et prévenir « si possible » la rupture díélasticité (rejet des demandes).


    .. section:: RB4FASTSIM
        :class: description

        **Bases réduites certifiées et non intrusives massivement parallèles pour la simulation de modèle multi-physiques non-linéaires**

        Dans ce projet nous nous interessons aux méthodes des bases réduites dans leur version intrusive (certifiée) et faiblement intrusive (Y. Maday. R. Chakir) et à leur application à des problèmes multi-physiques non-linéaires 3D nécessitant des ressources de calcul haute performances. Les modèles considérés sont ceux du LNCMI (thermique, electrostatique, magnetostatique, mécanique des solides) et díEADS-IW(aérothermique). En particulier, outre la parallélisation des modèles même, on síintéressera à la parallélisation des méthodes des bases réduites.


    .. section:: ViVaBrain
        :class: description

        **Écoulements vasculaires**

        On s'intéresse à la simulation díécoulements sanguins dans des géométries complexes 3D issues de líimagerie médicale. Par exemple, líimage ci-contre représente un réseau veineux dans le cerveau avec un peu plus de 30 entrées et 2 sorties (on peut considérer également ce réseau comme un réseau artériel), cíest une des géométries sur lesquelles nous porterons nos efforts. Nous nous intéresserons en particulier à la problématique des conditions aux limites, à la génération du maillage aussi bien dans le contexte d'écoulements fluide seul que dans le contexte d'interaction fluide-structure, à la résolution efficace en parallèle de líécoulement sanguin voire de líinteraction fluide-structure en utilisant Feel++.


    .. section:: VOG
        :class: description

        **Vlasov On Gpu**

        L'équation de Vlasov est un modèle cinétique qui décrit l'évolution d'une fonction de distribution f = f(x,v,t), avec x la variable spatiale, v la variable de vitesse et t le temps. La fonction de distribution peut, par exemple, représenter la répartition des électrons dans un plasma. Dans ce cas, l'équation de Vlasov peut être couplée aux équations de Maxwell pour calculer l'évolution du plasma. Deux grandes familles de schémas numériques existent pour discrétiser l'équation de Vlasov: les méthodes Particle in Cell (PIC) d'une part et les méthodes eulériennes d'autre part. Ce projet se place dans la deuxième catégorie, puisqu'on se propose d'utiliser une grille de l'espace des phases (x, v) pour discrétiser l'équation de Vlasov.



.. section:: Séminaires de la session de recherche
    :class: Description

    .. section:: Semaine 1
        :class: description

        Les séminaires de la première semaine de la session ont été dédiés à des cours sur la programmation parallèle donnés par Dimitri Lecas, CNRS/IDRIS.

        - `High Performance Architectures and Introduction to Parallel Programming <attachments/spip/Documents/Ecoles/CEMRACS2012/intro_en.pdf>`__
        - `MPI <attachments/spip/Documents/Ecoles/CEMRACS2012/mpi_en.pdf>`__
        - `OpenMP Programming <attachments/spip/Documents/Ecoles/CEMRACS2012/IDRIS_OpenMP_cours_en.pdf>`__
        - `Hybrid Programming <attachments/spip/Documents/Ecoles/CEMRACS2012/hybride_en.pdf>`__

    .. section:: Semaine 2
        :class: description

        - 30/07 : *An Embedded Boundary Method for Compressible Viscous Flows and Fluid/Structure Interaction Problems*, **Adam Larat** , EM2C, France
        - 31/07 : `Sparse direct solver on top of large-scale multicore systems with GPU accelerators <attachments/spip/Documents/Ecoles/CEMRACS2012/cemracs_PRamet.pdf>`__, **Pierre Ramet** , Labri, France
        - 01/08 : `C++ for beginners <attachments/spip/Documents/Ecoles/CEMRACS2012/CoursPOO_CEMRACS_1.pdf>`__ , **Pascal Havé** , IFP Energies Nouvelles, France
        - 02/08 : *Numerical schemes and parallel algorithms for Tokamak kinetic modeling* , **Virgine Grandgirard/Guillaume Latu** , CEA, France
        - 03/08 : **Stéphane Cordier** , MAPMO, France

    .. section:: Semaine 3
        :class: description

        - 06/08 : *Optimisation of large 3D flows : a few problems on bluff body flows and biological flows* , **Philippe Poncet** , INSA de Toulouse, France
        - 07/08 : `Advance C++ Part 1 (méta-programing) <attachments/spip/Documents/Ecoles/CEMRACS2012/CoursPOO_CEMRACS_2.pdf>`__ , **Pascal Havé** , IFP Energies Nouvelles, France
        - 08/08 : `Skew-Symmetric Schemes for compressible and incompressible flows <attachments/spip/Documents/Ecoles/CEMRACS2012/Julius_Reiss.pdf>`__ , **Julius Reiss** , TU-Berlin, Germany
        - 09/08 : `Advance C++ Part 1 (C++11) <attachments/spip/Documents/Ecoles/CEMRACS2012/CoursPOO_CEMRACS_3.pdf>`__ , **Pascal Havé** , IFP Energies Nouvelles, France `Exemples des cours C++ <attachments/spip/Documents/Ecoles/CEMRACS2012/CoursPOO_CEMRACS_examples.tgz>`__
        - 09/08 : *Introduction à FreeFem++* , **Georges Sadaka** , Université de Picardie Jules Verne, France
        - 10/08 : `Vertex centred discretization of two phase Darcy flows with discontinuous capillary pressures <attachments/spip/Documents/Ecoles/CEMRACS2012/VagCemracs.pdf>`__, **Roland Masson** , Université de Nice Sophia-Antipolis, France

    .. section:: Semaine 4
        :class: description

        - 13/08 : *C1-finite elements and applications to MHD.* **Boniface Nkonga** , Université de Nice Sophia Antipolis, France
        - 14/08 : *The Fastest Convolution in the West.* **Malcolm Roberts** , University of Alberta, Canada
        - 16/08 : *Laplacian on resistive networks and trees: models and computational issues*. **Bertrand Maury** , Université Paris Sud, France
        - 17/08 : *Jet-Schemes and Correction Function Methods for Incompressible Two-Phase Flows*, **Jean-Christophe Nav** , McGill University, Canada

    .. section:: Semaine 5
        :class: description

        - 20 et 21/08: `Workshop Maths-Entreprises <http://www.agence-maths-entreprises.fr/a/?q=fr/node/166>`__
        - 22/08: `Debian for Simulation and Numerical Modeling Applications to High Magnetic Field Magnets Design <attachments/spip/Documents/Ecoles/CEMRACS2012/trophime.pdf>`__, **Christophe Trophime** , CNRS Grenoble, France
        - 22/08 et 23/08 : Restitution des projets



.. section:: Communications médiatiques
    :class: description

    Le CEMRACS 2012 a fait l'objet de plusieurs communications médiatiques:

    - Le `communiqué de presse <attachments/spip/Documents/Ecoles/CEMRACS2012/cdp_cirm.pdf>`__ du CIRM
    - Un `article <attachments/spip/Documents/Ecoles/CEMRACS2012/laprovence.png>`__ paru dans la Provence
    - Un `article <attachments/spip/Documents/Ecoles/CEMRACS2012/LaMarseillaise.pdf>`__ paru dans la Marseillaise
    - Un `reportage <attachments/spip/Documents/Ecoles/CEMRACS2012/cemracs-france3-jt-19-20-provence-alpes-2012-08-16-18h59.ogv>`__ diffusé sur France 3

    Plusieurs reportages sont également diffusés sur la chaîne YouTube du CIRM:

    - `CEMRACS 2012 at CIRM <http://www.youtube.com/watch?v=lNsZ530RJsc&feature=plcp>`__
    - `Le CIRM, écrin estival du CEMRACS <http://www.youtube.com/watch?v=dBW2dcgkjdg&feature=plcp>`__
    - `La simulation numérique en médecine : les maths pour comprendre les AVC <http://www.youtube.com/watch?v=WwUIBrhj0w0&feature=plcp>`__
    - `Pourquoi ils ont choisi les maths <http://www.youtube.com/watch?v=kdgpIzG61u8&feature=plcp>`__
    - `Reportage de France 3 au CIRM <http://www.youtube.com/watch?v=n4K7Cl9XgXQ&feature=plcp>`__


.. section:: Comité scientifique
    :class: orga

    - Rémi Abgrall
    - Luc Giraud
    - Petros Koumoutsakos
    - Pauline Lafitte
    - Stéphane Lantéri
    - Jean Roman
    - Romain Teyssier


.. section:: Comité d'organisation
    :class: orga

    - Stéphane Descombes
    - Bernard Dussoubs
    - Sylvain Faure
    - Loïc Gouarin
    - Violaine Louvet
    - Marc Massot
    - Vincent Miele

Congrès SMAI 2013 : mini-symposium "Estimation de paramètres pour les EDP"
##########################################################################

:date: 2013-06-04 08:07:45
:modified: 2013-06-04 08:07:45
:category: journee
:tags: smai
:place: Seignosse
:start_date: 2013-05-30
:end_date: 2013-05-30
:summary: Le Groupe Calcul et le GdR Mascot-Num se sont associés pour co-organiser un mini-symposium au congrès SMAI 2013 sur l'estimation de paramètres pour les EDP.


.. section:: Mini-symposium
    :class: description

    Le Groupe Calcul et le GdR `Mascot-Num <http://www.gdr-mascotnum.fr>`__ se sont associés pour co-organiser un mini-symposium au congrès SMAI 2013 sur l'estimation de paramètres pour les EDP.

    La problématique générale de l'estimation de paramètres suppose le plus souvent un grand nombre d'évaluation du problème considéré, engendrant un coût calcul qui peut être hors de portée selon la complexité du modèle. Plusieurs voies de recherche s'attaquent au problème selon des angles différents, allant de la réduction de modèles à des approches plus statistiques. Ce mini-symposium s'intéresse plus particulièrement aux approches couplées edp-statistiques, les exposés exploreront différents points de vue au travers d'applications variées.

    Une description détaillée se trouve `ici <attachments/spip/Documents/Manifestations/SMAI2013/smai-espaedp-descriptions.pdf>`__


.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 30-05-2013

            .. event:: Introduction
                :begin: 10:45
                :end: 10:55
                :support: attachments/spip/Documents/Manifestations/SMAI2013/espaedp.pdf

            .. event:: Estimation statistique des paramètres pour des EDP de type transport-fragmentation
                :begin: 10:55
                :end: 11:15
                :speaker: Marc Hoffmann, Paris Dauphine, CEREMADE
                :support: attachments/spip/Documents/Manifestations/SMAI2013/hoffmann.pdf

                Nous  considérons sous l'angle statistique la reconstruction des paramètres d'une équationde type transport-fragmentation, dont un des champs d'applications est la modélisation de l'évolution de populations cellulaires structurées en taille, âge ou facteur de croissance. Sur plusieurs exemples, nous expliquerons comment le schéma d'observation accessible au statisticien pour le même problème de modélisation sous-jacent modifie le type d'équations que l'on est amené à considérer. Dans ce contexte, nous caractériserons les vitesses d'estimation et fournirons des procédures d'estimation non-paramétriques optimales. Ce travail est en collaboration avec Marie Doumic (INRIA et Laboratoire J.L. Lions).


            .. event:: Algorithmes gloutons pour la réduction de modèle
                :begin: 11:15
                :end: 11:35
                :speaker: Tony Lelièvre, École des Ponts ParisTech, CERMICS
                :support: attachments/spip/Documents/Manifestations/SMAI2013/lelievre.pdf

                Nous présenterons une famille d'algorithmes gloutons qui ont été proposés par F. Chinesta et A. Nouy pour approcher la solution de problèmes en grande dimension. Ces techniques s'appliquent en particulier à des équations aux dérivées partielles paramétrées. Nous présenterons les algorithmes et des développements récents concernant l'approximation du premier mode propre de problèmes elliptiques. Il s'agit d'un travail en commun avec Eric Cancès et Virginie Ehrlacher.


            .. event:: Couplage entre algorithme SAEM et précalcul pour la paramétrisation populationnelle d'équations de réaction diffusion
                :begin: 11:35
                :end: 11:55
                :speaker: Violaine Louvet, CNRS, Université Claude Bernard Lyon 1
                :support: attachments/spip/Documents/Manifestations/SMAI2013/louvet.pdf

                La paramétrisation populationnelle d'une ́équation aux dérivées partielles longue à calculer par un algorithme de type SAEM (utilisé par exemple dans Monolix) conduit à des temps de calcul prohibitifs. Il apparaît nécessaire de combiner SAEM avec un algorithme de réduction du temps de calcul pour seramener à des temps raisonnables. Dans cet exposé nous montrerons comment coupler des algorithmes de précalcul sur une grille avec SAEM et appliquerons ces stratégies à la paramétrisation de KPP.



            .. event:: Calibration d'un système d'EDP pour la régulation thermique au sein de la cabien d'un avion
                :begin: 11:55
                :end: 12:15
                :speaker: Nabil Rachdi, EADS
                :support: attachments/spip/Documents/Manifestations/SMAI2013/rachdibis.pdf

                Le groupe EADS est un leader mondial de l'aéronautique et de défense, dont ses activités dépendent fortement du développement et de l'intégration d'états de l'art et de nouvelles technologies dans ses produits afin de garantir la compétitivité nécessaire au sein de ses marchés. Cet exposé présente le problème de l'étude de confort au sein d'une cabine d'un avion commercial en phase de conception, ainsi que certains challenges scientifiques identifiés. Les échanges thermiques dans la cabine et la baie d'un avion sont modélisés par les ́équations de Navier-Stokes, qui sont implémentées dans un logiciel informatique. La résolution de telles équations, notamment les conditions limites, induit la présence de paramètres souvent mal connus. Il y a principalement deux types de tels paramètres : il y a ceux dont on connaît une valeur nominale mais qui sont sujets à une variabilité ou une incertitude intrinsèque (ex. le taux de turbulence, etc.), puis il y a ceux qui doivent être estimés (ex. résistance thermique de contact, conductivité thermique, etc.). L'estimation de ces derniers paramètres nécessite une information supplémentaire qui est en pratique la disponibilité de données d'avions antérieurs, d'expériences réelles, d'essais en chambre, etc. Bien souvent, les méthodes d'estimation peuvent s'avérer gourmandes en temps de calcul, notamment dû au coût de simulation du système EDP. Des techniques de réduction de modèle seront envisagées pour contourner cette difficulté. Les méthodes présentées seront illustrées sur un cas test industriel.



.. section:: Comité d'organisation
    :class: orga

    - Violaine Louvet, CNRS, Université Lyon 1, Institut Camille Jordan
    - Clémentine Prieur, Université Joseph Fourier, Laboratoire Jean Kuntzmann
    - Laurence Viry, Université Joseph Fourier, Laboratoire Jean Kuntzmann

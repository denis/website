Tinker-HP
##########################################################

:date: 2020-12-10 10:00:00
:category: cafe
:tags: visio
:start_date: 2020-12-10 10:00:00
:end_date: 2020-12-10
:place: En ligne
:summary: Présentation du logiciel Tinker-HP qui offre un environnement de calcul haute performance pour la modélisation de systèmes complexes de millions d'atomes.
:inscription_link: https://indico.mathrice.fr/event/232
:calendar_link: https://indico.mathrice.fr/export/event/232.ics

.. contents::

.. section:: Description
    :class: description
    
      Jean-Philip Piquemal présentera le logiciel Tinker-HP qui offre un environnement de calcul haute performance pour la modélisation de systèmes complexes de millions d'atomes. Il s'agit d'un outil extrêmement rapide pour réaliser les simulations de dynamique moléculaire qui permettent de comprendre les mécanismes d'infection virale et notamment ceux du COVID-19. Ce logiciel, écrit en Fortran, est massivement parallèle et capable d'exploiter les architectures GPU. Pour plus d'informations, allez sur le site http://tinker-hp.ip2ct.upmc.fr

      Cette présentation d'une durée de 30-40 minutes sera suivie d'une séance de questions.

      Cette présentation sera accessible aux non-chimistes et aura lieu sur la plateforme BBB de Mathrice. Pour des raisons techniques nous sommes obligés de limiter le nombre de participants et nous vous demandons de bien vouloir vous inscrire.



.. section:: Orateur
    :class: orateur

      - Jean-Philip Piquemal (Sorbonne Université)
      

Ecole d'automne "Informatique Scientifique" module1
###################################################

:date: 2008-05-19 16:21:34
:modified: 2008-05-19 16:21:34
:category: formation
:start_date: 2008-09-29
:end_date: 2008-10-03
:place: Sète, France
:summary: Cette formation est le premier module de l'école d'automne "Informatique Scientifique" qui avait pour but d’aider les personnes pratiquant le calcul scientifique dans leur activité quotidienne à mieux utiliser les outils numériques et informatiques existants pour développer et optimiser leurs codes de calcul.


.. contents::

.. section:: Description
    :class: description

    Cette formation avait pour but d’aider les personnes pratiquant le calcul scientifique dans leur activité quotidienne à mieux utiliser les outils numériques et informatiques existants pour développer et optimiser leurs codes de calcul.

    Le but était de détailler de manière théorique puis pratique le lien entre les méthodes numériques, les algorithmes, l’architecture des machines et la programmation. L’idée n'était pas d’apprendre un langage de programmation ou d’apprendre à utiliser une bibliothèque de calcul mais de savoir trouver ou implémenter les bons outils en fonction du problème numérique à traiter.


.. section:: Programme
    :class: programme

        .. schedule::

            .. day:: 29-09-2008

                .. event:: Architecture des ordinateurs
                    :begin: 09:00
                    :end: 10:30
                    :speaker: Françoise Berthoud, Violaine Louvet, Françoise Roch
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/2008/m1/architecture.pdf)

                    - Architecture machine
                    - Problème des accès mémoire (séquentiel, aléatoire)
                    - Cache
                    - Processus
                    - Nouveaux processeurs
                    - Nouvelles architectures

                .. event:: Mécanismes automatiques d'optimisation des bibliothèques
                    :begin: 10:30
                    :end: 12:00
                    :speaker: Thierry Dumont
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/2008/m1/optim-bibs.pdf)

                    Comment sont optimisées les librairies de type ATLAS, FFTW, ...

                .. event:: Systèmes Linéaires I
                    :begin: 14:00
                    :end: 16:00
                    :speaker: Pierre Ramet
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/2008/m1/SL1.pdf)

                    - A.L. dense,
                    - algorithmes de factorisation,
                    - (Sca)LAPACK,
                    - structures de données par bloc,
                    - parallélisation ...

                .. event:: Systèmes Linéaires II
                    :begin: 16:00
                    :end: 18:00
                    :speaker: Abdou Guermouche
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/2008/m1/SL2.pdf)

                    - A.L. creuse,
                    - méthodes directes (multifrontale, supernodale),
                    - pb de numérotation,
                    - les principales étapes,
                    - parallélisation ...

            .. day:: 30-09-2008

                .. event:: Systèmes linéaires III
                    :begin: 09:00
                    :end: 12:00
                    :speaker: Pascal Hénon
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/2008/m1/SL3.pdf)

                    - A.L. creuse,
                    - méthodes itératives,
                    - introduction aux méthodes de type Krylov,
                    - factorisations incomplètes (ILU(k), ILU(t)),
                    - parallélisation ...


                .. event:: Méthodes de type multigrille
                    :begin: 14:00
                    :end: 17:30
                    :speaker: Thierry Dumont
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/2008/m1/multigrilles.pdf)

                    Multigrilles géométriques et algébriques.

            .. day:: 01-10-2008

                .. event:: Compilation I
                    :begin: 09:00
                    :end: 12:00
                    :speaker: Romaric David
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/2008/m1/Compilation-pres.pdf)

                    - Rappels sur le compilateur.
                    - Optimisations réalisées option compil / Optim (ex : sse)
                    - Les différentes familles de compilateurs (Libres GNU, Commerciaux Intel + Portland)
                    - Options de compilation fréquemment utilisées
                    - Utilisation des directives de compilation

                .. event:: Compilation II
                    :begin: 14:00
                    :end: 17:30
                    :speaker: Romaric David
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/2008/m1/Linkage-pres.pdf)

                    - Rappels sur la compilation séparée
                    - Construction des lib dynamiques et statiques
                    - libtool
                    - Editions de liens à la compilation
                    - Que fait le système lors de l’exécution d’un programme ? Variables influant la recherche de lib. à l’exécution.
                    - Configuration système


            .. day:: 02-10-2008

                .. event:: Introduction générale sur les langages utilisés en calcul scientifique
                    :begin: 09:00
                    :end: 10:30
                    :speaker: Romaric David
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/2008/m1/langages.pdf)

                    La problématique de l'interfaçage de ces langages.

                .. event:: C++ appliqué au calcul scientifique
                    :begin: 10:30
                    :end: 12:00
                    :speaker: Stéphane Labbé
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/2008/m1/C++.pdf)

                    Trucs et astuces, à faire et à ne pas faire. Les problèmes de pointeurs, les recettes pour les numériciens.

                .. event:: Fortran2003
                    :begin: 14:00
                    :end: 16:30
                    :speaker: Laurence Viry
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/2008/m1/f2003.pdf)

                    Les nouveautés de la norme 2003 pour le calcul scientifique.

                .. event:: Python
                    :begin: 16:30
                    :end: 18:00
                    :speaker: Romaric David
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/2008/m1/Python-pres.pdf)

                    Utilisation en calcul et interfaçage avec d'autres langages.

            .. day:: 03-10-2008

                .. event:: Les outils standards de construction de programmes
                    :begin: 09:00
                    :end: 10:30
                    :speaker: Jean-Marc Muller
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/2008/m1/outils-pg.pdf)

                    Makefile, autotools, soncs, cmake, ...

                .. event:: Les outils de gestion de développement
                    :begin: 10:30
                    :end: 12:00
                    :speaker: Jean-Marc Muller
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/2008/m1/outils-gestion.pdf)

                    AGL (Ateliers de Génie Logiciels), gestions de versioni, ...


.. section:: Intervenants
    :class: orga

    - Françoise Berthoud
    - Romaric Davic
    - Thierry Dumont
    - Abdou Guermouche
    - Pascal Hénon
    - Laurence Viry
    - Stéphane Labbé
    - Violaine Louvet
    - Jean-Marc Muller
    - Pierre Ramet
    - Françoise Roch

.. section:: Responsables scientifiques
    :class: orga

    - Thierry Dumont
    - Violaine Louvet

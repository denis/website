ANF "R pour le calcul" : R avancé et performances
#################################################

:date: 2015-04-30
:category: formation
:tags: R
:authors: Vincent Miele
:start_date: 2015-10-04
:end_date: 2015-10-09
:place: Aussois

.. contents::

.. section:: Description
  :class: description

  Nous proposons un enseignement de connaissances indispensables à la maîtrise
  du fonctionnement de R, pour favoriser son utilisation au plus près de la
  performance machine. Les participant/e/s seront sensibilisé/e/s aux bonnes
  pratiques de développement de codes et de packages en R.  A l’issue de l'ANF,
  les participant/e/s seront capables d'associer la souplesse et l'exhaustivité
  méthodologique de R à des garanties de performances calculatoires.

  Cette ANF aura eu lieu du dimanche 04 octobre au vendredi 09 octobre au
  centre Paul Langevin à Aussois.

  .. figure:: attachments/spip/IMG/png/aussois.png
     :alt: Photo du groupe de participants à Aussois
     :width: 100%
     :align: center


  Cette ANF s'adresse aux développeurs R non débutants.  Elle est gratuite
  (inscription+hébergement/repas) et ouverte aux personnels (statutaires, doctorant/e/s ou CDD)

    - du CNRS
    - universitaires des UMR CNRS
    - de l'INRA
    - de l'INSERM (pour les autres EPIC, nous contacter) }}

  Les sessions seront réalisées en français. Chaque participant/e devra
  disposer d'un ordinateur portable (de préférence sous Linux ou MacOSX),
  avec une version récente de R (>3.0).

.. section:: Programme
  :class: programme

  .. schedule::

    .. day:: 05-10-2019

      .. event:: Présentation + lancement du bookcrossing
        :begin: 09:00
        :end: 10:30

      .. event:: Introduction aux langages, architecture des machines et au calcul
        :begin: 11:00
        :end: 12:30
        :speaker: Violaine Louvet
        :support: attachments/spip/IMG/pdf/cours2.pdf

      .. event:: R avancé (partie 1),
        :begin: 14:00
        :end: 17:30
        :speaker: Romain François (r-enthusiasts.com)
        :support: attachments/spip/IMG/pdf/aussois-r-avance-1.pdf

    .. day:: 06-10-2019

      .. event:: R, histoire et écosystème
        :begin: 09:00
        :end: 11:00
        :speaker: Martyn Plummer (IARC, R core group)
        :support: attachments/spip/IMG/pdf/r-history-ecology.pdf

      .. event:: Calcul parallèle avec R (partie 1)
        :begin: 11:30
        :end: 12:30
        :speaker: Vincent Miele (LBBE)
        :support: attachments/spip/IMG/pdf/tutoriel-parallel-miele.pdf

      .. event:: Calcul parallèle avec R (partie 2)
        :begin: 14:00
        :end: 16:00
        :speaker: Vincent Miele (LBBE)
        :support: attachments/spip/IMG/pdf/tp-parallel-miele.pdf

      .. event:: Debogage de code R
        :begin: 16:30
        :end: 17:30
        :speaker: Vincent Miele (LBBE)
        :support:
          [Archive 1](attachments/spip/IMG/zip/hclustdemo-3.zip)
          [Archive 2](attachments/spip/IMG/gz/hclustdemo_0.0.tar.gz)
          [Cours](attachments/spip/IMG/pdf/tutoriel-debgprof.pdf)
          [TP](attachments/spip/IMG/pdf/tp-debgprof.pdf)

    .. day:: 07-10-2019

      .. event:: Systèmes de gestion de version
        :begin: 09:00
        :end: 09:15
        :speaker: Florent Langrognet (LMB)

      .. event:: Appel aux langages compilés / Rcpp
        :begin: 09:15
        :end: 12:30
        :speaker: Romain François (r-enthusiasts.com)
        :support: attachments/spip/IMG/pdf/rcpp.pdf

      .. event:: Profilage de code R
        :begin: 14:00
        :end: 16:00
        :speaker: Vincent Miele (LBBE)

    .. day:: 08-10-2019

      .. event:: R avancé (partie 2)
        :begin: 09:00
        :end: 12:30
        :speaker: Romain François (r-enthusiasts.com)
        :support:
          [Fonctions](attachments/spip/IMG/pdf/r-avance-partie-2.pdf)
          [Environnements](attachments/spip/IMG/pdf/r-avance-environnements.pdf)
          [Programmation fonctionnelle](attachments/spip/IMG/pdf/r-avance-prog_fonctionnelle.pdf)

      .. event:: Développement de packages R
        :begin: 14:00
        :end: 17:30
        :speaker: Aurélie Siberchicot (LBBE)
        :support:
          [Cours](attachments/spip/IMG/pdf/rpackages_anfrcalcul_102015.pdf)
          [Archive](attachments/spip/IMG/zip/PackagesR.zip)

    .. day:: 09-10-2019

      .. event:: Visualisation de données
        :begin: 08:30
        :end: 11:30
        :speaker: Eric Matzner-Lober (Université de Rennes)
        :support:
          [Cours ggplot](attachments/spip/IMG/pdf/presentationggplot.pdf)
          [Archive ggplot](attachments/spip/IMG/zip/presentationggplot.rmd.zip)
          [Cours visualisation 1](attachments/spip/IMG/pdf/visualisation1.pdf)
          [Cours visualisation 2](attachments/spip/IMG/pdf/visualisation2.pdf)


.. section:: Comité d'organisation et de programme
  :class: orga

    - `Florent Langrognet  <http://lmb.univ-fcomte.fr/Florent-Langrognet>`_
    - Violaine Louvet
    - `Vincent Miele <http://lbbe.univ-lyon1.fr/-Miele-Vincent-.html>`_
    - `Aurélie Siberchicot  <https://lbbe.univ-lyon1.fr/-Siberchicot-Aurelie-.html>`_

.. section:: Partenaires
  :class: orga

    - `CNRS  <http://www.cnrs.fr>`__
    - `INRA  <http://www6.inra.fr/pepi>`__
    - GdR BIM
    - `GdR EcoStat  <https://sites.google.com/site/gdrecostat/>`__
    - GdR Stat&Santé
    - `Laboratoire de Biométrie et Biologie Évolutive  <http://lbbe.univ-lyon1.fr>`__
    - `Laboratoire Mathématiques Besançon  <https://lmb.univ-fcomte.fr>`__
    - `SFdS  <http://www.sfds.asso.fr>`__
    - `SMAI  <http://smai.emath.fr>`__
    - `Pepi INRA IBIS  <https://www6.inra.fr/pepi/Ingenierie-Bio-Informatique-et-Statistique-pour-les-donnees-haut-debit-IBIS>`__
    - `LyonCalcul  <http://lyoncalcul.univ-lyon1.fr/>`__
    - `RLyon  <http://www.meetup.com/R-Lyon/>`__

    |

    .. container:: text-align-center

        .. image:: attachments/spip/IMG/png/gdr-bim-logo-small.png
           :alt: Logo GdR BIM
        .. image:: attachments/spip/IMG/jpg/sfds_logo_small.jpg
           :alt: Logo SFDS
        .. image:: attachments/spip/IMG/jpg/smai_small.jpg
           :alt: Logo Smai

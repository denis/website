Atelier Optimisation
####################

:date: 2019-06-20 09:10:03
:category: formation
:tags: optimisation, architecture
:start_date: 2019-05-16
:end_date: 2019-05-17
:place: Saclay
:summary: Voir (ou revoir) les bases de l’optimisation de code
:inscription_link: https://indico.mathrice.fr/event/167/registration

.. contents::

.. section:: Description
    :class: description
    
    Vous aimeriez que vos programmes fassent meilleur usage des ressources matérielles, mais le domaine de la performance logicielle vous semble nébuleux et vous peinez à trouver une introduction pour vous mettre le pied à l'étrier ?

    Cet atelier organisé par DevLog et le Groupe Calcul est fait pour vous !
    
    Nous y dresserons un panorama théorique et pratique du sujet qui vous aidera à répondre aux questions suivantes :
    
    - Quelles caractéristiques du matériel informatique limitent les performances des applications ?
    - Quelles facettes des langages de programmation ont une influence sur les performances ?
    - Comment peut-on quantifier, mesurer et analyser la performance d'un programme ?
    - Comment peut-on organiser son développment pour maximiser les performances en minimisant l'effort associé ?
    
    Ce socle de culture générale vous donnera les bases nécessaires pour commencer à optimiser vos programmes, et pour aborder selon vos besoins les formations plus avancées qui approfondissent différents aspects de ces questions.
    
    **Prérequis**
    
    - Être autonome en environnement Linux / shell bash
    - Avoir des notions d'algorithmique

        - Complexité asymptotique
        - Structures de données courantes (tableaux, listes...)
    - Avoir une expérience de programmation en C, Fortran ou Python
    
    Par exemple, être capable de comprendre sans effort le code suivant :
    
    .. code-block:: C

        #include <stddef.h>
        void mystere(double* a, const double* b, size_t n) {
            size_t i;
            for (i = 0; i < n; ++i) {
                a[i] += a[i] * b[i];
            }
        }

    Nombre de participants : 31

.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 16-05-2019
                
                .. break_event:: Accueil
                    :begin: 13:30
                    :end: 14:00
        
                
                
                .. event:: Que cherche-t-on à optimiser ?
                    :begin: 14:00
                    :end: 15:30
                    :speaker: Matthieu Boileau
                    :support:
                        [support 1](https://groupe-calcul.pages.math.unistra.fr/collaboration_devlog_projet_2019/optimisation/intro/#/)
                        [support 2](attachments/evt/2019-05-atelier-optimisation/support09.pdf)
        
                    
                    
                
                .. break_event:: Pause café
                    :begin: 15:30
                    :end: 16:00
        
                
                
                .. event:: Langages et performance
                    :begin: 16:00
                    :end: 17:30
                    :speaker: Hadrien Grasland
                    :support:
                        [support 1](https://groupe-calcul.pages.math.unistra.fr/collaboration_devlog_projet_2019/optimisation/langages/Langages.html#/)
                        [support 2](attachments/evt/2019-05-atelier-optimisation/support02.pdf)
        
                    
                    
        .. day:: 17-05-2019
                
                .. event:: Caractéristiques de performance du matériel
                    :begin: 09:00
                    :end: 10:30
                    :speaker: Matthieu Haefele
                    :support:
                        [support 1](https://groupe-calcul.pages.math.unistra.fr/collaboration_devlog_projet_2019/optimisation/architecture_bottlenecks/computer_architectures.html#/)
                        [support 2](attachments/evt/2019-05-atelier-optimisation/support10.pdf)
        
                    
                    
                
                .. break_event:: Pause café
                    :begin: 10:30
                    :end: 11:00
        
                
                
                .. event:: Analyser la performance d'un programme [1/2]
                    :begin: 11:00
                    :end: 12:30
                    :speaker: Anne Cadiou
                    :support:
                        [support 1](attachments/evt/2019-05-atelier-optimisation/support04.pdf)
        
                    
                    
                
                .. break_event:: Pause déjeuner
                    :begin: 12:30
                    :end: 14:00
        
                
                
                .. event:: Analyser la performance d'un programme [2/2]
                    :begin: 14:00
                    :end: 15:30
                    :speaker: Hadrien Grasland
                    :support:
                        [support 1](https://gitlab.com/acadiou/TP_Profiling)
                        [support 2](attachments/evt/2019-05-atelier-optimisation/support08.tgz)
        
                    
                    
                
                .. break_event:: Pause café
                    :begin: 15:30
                    :end: 16:00
        
                
                
                .. event:: Une démarche globale d'optimisation de performances
                    :begin: 16:00
                    :end: 17:00
                    :speaker: Hadrien Grasland
                    :support:
                        [support 1](https://groupe-calcul.pages.math.unistra.fr/collaboration_devlog_projet_2019/optimisation/conclusion/conclusion.html#/)
                        [support 2](attachments/evt/2019-05-atelier-optimisation/support07.pdf)
        
                    
                    

.. section:: Organisation
    :class: orga

        - Matthieu Boileau
        - Anne Cadiou
        - Bastien Di Pierro
        - Hadrien Grasland
        - Matthieu Haefele

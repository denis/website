Interopérabilité et pérennisation des données de la recherche : comment FAIR En pratique ?
##########################################################################################

:date: 2018-09-28 17:41:31
:category: journee
:authors: Anne Cadiou
:start_date: 2018-11-27
:end_date: 2018-11-27
:place: Paris

.. contents::

.. section:: Description
    :class: description


    **Contexte**

    Avec le mouvement de l’ouverture des données de la recherche et du partage dit “Science Ouverte”, au niveau européen et international, de nombreux pays se sont engagés dans des politiques permettant le partage des résultats scientifiques et des données. La Science Ouverte est une nouvelle approche de la démarche scientifique, basée sur la production collaborative des produits de science, de leur partage, de leur libre circulation et réutilisation. La Commission européenne a émis dès 2012 des recommandations concernant la diffusion des résultats scientifiques. Elles invitent les chercheurs à s’appuyer sur les principes FAIR (Findable, Accessible, Interoperable, Reusable) permettant de favoriser la découverte, l’accès, l’interopérabilité et la réutilisation des données. Ils permettent de guider les stratégies de gestion des données et d’aider tous les acteurs qui œuvrent à les produire, à en contrôler la qualité, à les traiter et les analyser, à assurer leur publication et leur dissémination, à les sélectionner et les préparer pour le dépôt dans des plateformes de partage ou d’archivage.

    **Objectifs**

    Organisée par le groupe de travail inter-réseaux « Atelier données » (auquel participe Calcul) et financée par Mission pour les Initiatives transverses et interdisciplinaires (MITI), la journée d’étude a pour objectif de présenter des retours d’expériences et des réflexions sur les pratiques de gestion des données de la recherche mises en œuvre par les réseaux métiers et les réseaux technologiques du CNRS. Elle s’appuiera plus spécifiquement sur les notions de pérennisation et d’interopérabilité des données dans les projets de recherche, d’en comprendre les facteurs ressorts de réussite et les points sensibles à surveiller.

    Cette journée a pour ambition :

    - d’analyser les complémentarités de ces expériences au travers des métiers représentés par les réseaux ;
    - de formuler des points de convergence de bonnes pratiques ;
    - d’accroître les échanges entre les réseaux de la MI sur des questions à forts enjeux pour l’évolution de nos métiers.

    Cette journée est organisée avec le soutien de la DIST du CNRS.

    A noter : la journée sera webcastée par l’équipe du CC-IN2P3.

    **Site web**

    https://gt-donnees2018.sciencesconf.org/


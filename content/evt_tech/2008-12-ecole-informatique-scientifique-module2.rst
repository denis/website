Ecole d'automne "Informatique Scientifique" module 2
####################################################

:date: 2008-05-19 16:21:34
:modified: 2008-05-19 16:21:34
:category: formation
:start_date: 2008-12-01
:end_date: 2008-12-05
:place: Sète, France
:summary: Cette formation est le deuxième module de l'école d'automne "Informatique Scientifique" qui avait pour but d’aider les personnes pratiquant le calcul scientifique dans leur activité quotidienne à mieux utiliser les outils numériques et informatiques existants pour développer et optimiser leurs codes de calcul.


.. contents::

.. section:: Description
    :class: description

    Cette formation avait pour but d’aider les personnes pratiquant le calcul scientifique dans leur activité quotidienne à mieux utiliser les outils numériques et informatiques existants pour développer et optimiser leurs codes de calcul.

    Le but était de détailler de manière théorique puis pratique le lien entre les méthodes numériques, les algorithmes, l’architecture des machines et la programmation. L’idée n'était pas d’apprendre un langage de programmation ou d’apprendre à utiliser une bibliothèque de calcul mais de savoir trouver ou implémenter les bons outils en fonction du problème numérique à traiter.


.. section:: Programme
    :class: programme

        .. schedule::

            .. day:: 01-12-2008

                .. event:: Quelques modèles mathématiques et une idée de leur discrétisation
                    :begin: 09:00
                    :end: 12:00
                    :speaker: Thierry Dumont
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/2008/m2/modelisation.pdf)
                        [Cours détaillé](attachments/spip/Documents/Ecoles/2008/m2/modelisation-cours.pdf)
                        [Introduction au problème de réaction-diffusion](attachments/spip/Documents/Ecoles/2008/m2/intro-RD.pdf)

                    - Equation de la chaleur et variante
                    - Lois de conservation
                    - Discrétisation

                .. event:: Résolution de systèmes d'équations différentielles ordinaires non raides et raides : tour des méthodes les plus efficaces
                    :begin: 14:00
                    :end: 18:00
                    :speaker: Stéphane Descombes, Marc Massot
                    :support:
                        [Cours EDO non raides](attachments/spip/Documents/Ecoles/2008/m2/cours-ODE.pdf)
                        [Cours EDO raides](attachments/spip/Documents/Ecoles/2008/m2/cours-ODE-raides.pdf)

                    - LSODE
                    - explicite d'ordre élevé
                    - Radau
                    - Euler
                    - Rosenbrock
                    - ...

            .. day:: 02-12-2008

                .. event:: Etudes des schémas temporels adaptés aux EDP paraboliques
                    :begin: 09:00
                    :end: 12:00
                    :speaker: Stéphane Descombes
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/2008/m2/cours-temps.pdf)

                    - BDF d'ordre 2
                    - implicite-explicite
                    - les idées liées au spliting


            .. day:: 03-12-2008

                .. event:: Profilage, debuggage, optimisation
                    :begin: 09:00
                    :end: 10:30
                    :speaker: Romaric David
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/2008/m2/pres-profil.pdf)
                        [Cours détaillé](attachments/spip/Documents/Ecoles/2008/m2/cours-profil.pdf)


                .. event:: Parallélisme : les différents types, liens avec l'architecture des machines
                    :begin: 10:30
                    :end: 12:00
                    :speaker: Laurence Viry, Violaine Louvet
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/2008/m2/parallelisme.pdf)


                .. event:: Boost.Threads
                    :begin: 14:00
                    :end: 16:00
                    :speaker: Thierry Dumont
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/2008/m2/boost-threads.pdf)


                .. event:: L'exemple de la combustion
                    :begin: 16:00
                    :end: 18:00
                    :speaker: Lucie Fréret
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/2008/m2/combustion.pdf)



            .. day:: 04-12-2008

                .. event:: OpenMP
                    :begin: 09:00
                    :end: 10:30
                    :speaker: Françoise Berthoud
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/2008/m2/openm-angd2008.pdf)


                .. event:: Décomposition de domaine
                    :begin: 10:30
                    :end: 12:00
                    :speaker: Jacques Laminie
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/2008/m2/Domain_decomposition.pdf)


                .. event:: MPI
                    :begin: 14:00
                    :end: 16:30
                    :speaker: Laurence Viry
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/2008/m2/mpi.pdf)


                .. event:: Python multi-domaines
                    :begin: 16:30
                    :end: 18:00
                    :speaker: Loïc Gouarin
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/2008/m2/python-multidomaines.pdf)


            .. day:: 05-12-2008

                .. event:: Introduction à la visualisation
                    :begin: 09:00
                    :end: 10:00
                    :speaker: Stéphane Marchesin
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/2008/m2/cours-paraview.pdf)


                .. event:: HDF5
                    :begin: 10:00
                    :end: 11:00
                    :speaker: Romaric David
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/2008/m2/hdf5.pdf)


                .. event:: VTK
                    :begin: 11:00
                    :end: 12:00
                    :speaker: Sylvain Faure
                    :support:
                        [Cours](attachments/spip/Documents/Ecoles/2008/m2/Presentation_VTK.pdf)


.. section:: Intervenants
    :class: orga

    - Françoise Berthoud
    - Romaric Davic
    - Stéphane Descombes
    - Thierry Dumont
    - Sylvain Faure
    - Lucie Fréret
    - Loïc Gouarin
    - Jacques Laminie
    - Violaine Louvet
    - Stéphane Marchesin
    - Marc Massot
    - Laurence Viry

.. section:: Responsables scientifiques
    :class: orga

    - Thierry Dumont
    - Violaine Louvet

Journée Runtime
###############

:date: 2017-01-20 15:55:49
:category: journee
:start_date: 2017-01-20
:end_date: 2017-01-20
:place: Paris

.. contents::

.. section:: Description
    :class: description

    Les architectures OpenPower et Many Integrated Cores (MIC) annoncées pour les systèmes HPC pre-exascale en 2018 ont d'ores et déjà fait leur apparition dans le paysage en 2016. L'utilisation efficace de ces architectures par les applications scientifiques comprend plusieurs défis à relever. Pour n'en citer que deux, une très grande scalabilité ainsi qu'un déploiement sur une architecture hétérogène sont indispensables.

    L'emploi d'un modèle de programmation par tâches couplé à un système "runtime" dans les applications scientifiques permet-il de relever ces défis ? Ceux qui ont tenté l'expérience, ont-ils été couronnés de succès ? Voici deux questions auxquelles cette journée tente de répondre en permettant de faire le point ou de découvrir ces modèles de programmation émergents.

    La journée aura lieu le 20 janvier 2017 au centre de recherche Inria de Paris (12ème arrondissement de Paris au 2 rue Simone Iff) salle JL Lions 1 au RDC du bâtiment C.


.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 20-01-2017

            .. break_event:: Accueil
                :begin: 09:00
                :end: 09:30

            .. event:: Task-Based Parallel Programming for HPC
                :begin: 09:30
                :end: 10:30
                :speaker: Olivier Aumage (Inria équipe-projet STORM)
                :support: attachments/spip/Documents/Journees/janv2017/tasks_programming_and_starpu.pdf

                Multicore processors, manycores accelerators and GPUs stress the ability of HPC applications to adapt, to a large, diverse set of possible hardware ressources. Task-based parallel programming models, introduced more than two decades ago, are becoming increasingly popular, due to their capacity of adaptiveness, and their suitability to be driven by many kinds of scheduling policies. This talk will provide a general introduction to task-based parallel programming, and discuss the properties of task programming models in the context of HPC.

            .. event:: StarPU: Un support d'exécution unifié pour les architectures hétérogènes
                :begin: 10:30
                :end: 11:30
                :speaker: Olivier Aumage (Inria équipe-projet STORM)
                :support: attachments/spip/Documents/Journees/janv2017/tasks_programming_and_starpu.pdf

                La complexité et l'hétérogénéité croissantes des plateformes de calcul intensif nécessitent un effort d'adaptation continuel des applications scientifiques afin de les exploiter. La proportion de code applicatif dédiée à la gestion des calculs augmente de concert. L'équipe STORM (Inria - LaBRI, Bordeaux) développe le support d'exécution StarPU afin de répondre à cette problématique. StarPU propose un modèle de programmation unifié à base de flot de tâches séquentiel, et un moteur d'exécution parallèle associé permettant de piloter le code applicatif sur des plateformes hétérogènes.

            .. event:: Improving an OpenMP runtime using XKaapi
                :begin: 11:30
                :end: 12:30
                :speaker: Thierry Gautier (Inria)
                :support: attachments/spip/Documents/Journees/janv2017/kaapi_libkomp.pdf

                Recently, Jack Dongarra saids  "...it is widely agreed that math libraries should, wherever possible, embrace event-driven and message-driven execution models, in which work is abstracted in the form of asynchronous tasks, whose completion can trigger additional computation tasks and data movements". The purpose of this talk is to give an overview of the roles a runtime should play in the context of HPC application.  We focus on the XKaapi runtime and how it manages DAG scheduling: the tasks’ creations and theirs dependencies, the way it schedules them by work stealing. XKaapi is a runtime able to schedule OpenMP programs on top of multicore and manycore architecture. Previous works has also show good support to heterogeneous multi-CPUs-multi-GPUs architecture. We detail an original application of the "work first principle” from Cilk to report overhead in task's creations to the critical path; and how to reduce remote memory accesses during steal operation by using a steal request combining algorithm. Performances on various applications accross the past years illustrates the presentation.
                Almost all key features in XKaapi are integrated in a modified Intel OpenMP runtime library.


            .. break_event:: Repas
                :begin: 12:30
                :end: 14:00

            .. event:: Implémentation du moteur exécutif KAAPI pour le parallélisme à mémoire partagée avec équilibrage par vol de travail dans le programme de simulation en dynamique rapide EUROPLEXUS
                :begin: 14:00
                :end: 14:30
                :speaker: Vincent Faucher (CEA Cadarache)
                :support: attachments/spip/Documents/Journees/janv2017/Journ&eacute;e%20runtime%20EPX%2020-01-2017%20-%20Faucher_v0.pdf

                Sur la base d'une collaboration CEA-INRIA née du projet ANR REPDYN (2010-2013) dédié au passage à l'échelle des méthodes numériques en dynamique rapide avec interaction fluide-structure, le recours au moteur exécutif KAAPI a été implémenté dans le programme EUROPLEXUS (http://www-epx.cea.fr) pour améliorer l'ordonnancement des tâches et optimiser l'extensibilité à mémoire partagée pour des modèles faisant intervenir classiquement des entités différentes au niveau du fluide et de la structure, avec des coûts de traitement très hétérogènes pris en compte par vol de travail.

                L'architecture cible correspond aux clusters de type pétaflopique, tel CURIE au TGCC, composés d'un agrégat de nœuds multi-cœurs, pour lesquels une approche hybride MPI/KAAPI est mise en œuvre. Des travaux de recherche (thèse entre 2013 et 2016) ont tenté d'intégrer une gestion multi-niveaux dans l'ordonnancement proposé par KAAPI pour coller à la structure interne des nœuds courants (multi-processeurs avec cache privé par processeur), avec des résultats encore à consolider.


            .. event:: Task based parallelization of recursive linear algebra routines using Kaapi
                :begin: 14:30
                :end: 15:00
                :speaker: Clément Pernet (IMAG)
                :support: attachments/spip/Documents/Journees/janv2017/runtime_pernet.pdf

                We will report on our experience in the development of the parallel code of FFLAS-FFPACK, a library for dense linear algebra over a finite field.

                The set of routines is similar to that of the numerical BLAS and LAPACK, working over exact arithmetic involves two main differences: the use of recursive algorithms to harness sub-cubic time matrix multiplication, and specific pivoting strategies handling rank deficiencies. The parallelization is therefore based on recursive tasks with dataflow dependencies. We will explain how this parallelization has been made effective and present comparison showing the strong benefit provided by KAAPI when dealing with numerous recursive tasks. This application also reveals several features that are currently not available in any runtime, such as the resolution of data dependencies through recursive call trees, which form a wish list for future developments of such runtimes.


            .. event:: QR mumps: a runtime-based Sequential Task Flow parallel solver
                :begin: 15:00
                :end: 15:30
                :speaker: Alfredo Buttari (CNRS-IRIT)
                :support: attachments/spip/Documents/Journees/janv2017/journee_runtimes_2017_alfredo_buttari.pdf

                QR mumps is a parallel, direct solver for sparse linear systems based on the multifrontal QR factorization. Parallelism is achieved using a Sequential Task Flow (STF) based model on top of the StarPU runtime system. In this talk we will show how STF parallelism can be applied to a sparse, direct solver and how the use of a modern runtime system allows for the portable and efficient implementation of complex algorithms that can improve its performance and scalability as well as its memory consumption.


            .. break_event:: Pause
                :begin: 15:30
                :end: 16:00


            .. event:: Evaluation d'une approche parallèle à base de tâches dans le solveur CFD Aghora pour des architectures hétérogènes
                :begin: 16:00
                :end: 16:30
                :speaker: Emeric Martin (ONERA)
                :support: attachments/spip/Documents/Journees/janv2017/Journée_Runtime_E.Martin.pdf

                L'Onera développe le code de calcul Aghora basé sur une discrétisation spatiale de type Galerkin discontinu pour la simulation numérique des écoulements compressibles turbulents en aérodynamique interne ou externe. Afin d'exploiter efficacement toute la puissance de calcul offerte par les plateformes modernes, une nouvelle approche parallèle à base de tâches a été introduite dans Aghora et repose sur la bibliothèque runtime StarPU. Tout d'abord, nous présenterons l'étape de restructuration du code indispensable à l'expression de ce formalisme de tâches. Ensuite, nous illustrerons l'influence sur la performance de la granularité des tâches et des politiques d'ordonnancement. Les comparaisons avec une approche classique MPI se feront sur un noeud composé de processeurs multi-core Intel et de co-processeurs many-core Intel Xeon Phi sur un cas représentatif d'écoulements turbulents compressibles.

            .. event:: HPX - Vers un runtime parallèle pour C++
                :begin: 16:30
                :end: 17:00
                :speaker: Joël Falcou (LRI)
                :support: attachments/spip/Documents/Journees/janv2017/parallelism_in_cpp_sc15.pdf

                Les besoins en termes de support de haut niveau pour le parallélisme dans des langages comme C++ sont devenus criants durant les 5 dernières années.

                HPX, développé par le Stellar Group de la Louisiana State University, se propose de répondre à ce besoin en se basant sur des choix techniques innovants mettant en avant la simplicité d'utilisation, la prépondérance des calculs asynchrones et l'interopérabilité avec le standard C++.

                Dans cette présentation, nous parcourrons les problèmes que HPX veut résoudre, les choix techniques effectués et quelques exemples applicatifs.


.. section:: Organisation
    :class: orga

    - Loic Gouarin (CNRS & Laboratoire de Mathématiques d'Orsay)
    - Matthieu Haefele (CNRS & Maison de la Simulation)
    - Michel Kern (Inria & Maison de la Simulation)

Title: Numerical simulation of the electrodeformation of blood cells. Application to medical diagnostics
Date: 2021-04-23 15:34
Slug: job_84d4dcfed6e84bfa9258b5dc5e693e88
Category: job
Authors: Simon Mendez
Email: simon.mendez@umontpellier.fr
Job_Type: Thèse
Tags: these
Template: job_offer
Job_Location: Institut Montpelliérain Alexander Grothendieck, Montpellier
Job_Duration: 3 years
Job_Website: https://imag.umontpellier.fr/~mendez/
Job_Employer: University of Montpellier
Expiration_Date: 2021-05-18
Attachment: job_84d4dcfed6e84bfa9258b5dc5e693e88_attachment.pdf

The details of the offer (manily on French) and the procedure for application may be found here:
<https://www.adum.fr/as/ed/voirproposition.pl?site=adumR&amp;matricule_prop=36310>

**Application deadline: 18 May 2021**

**Context: **

In many medical devices for diagnostics, flowing cells are subjected to an external electrical field. This field may polarize the cells and allow to displace and deform them: this is referred to as dielectrophoresis and electro-deformation, respectively. The field of applications is very broad: detection of cancer cells, cellular therapy, diagnosis of hematological diseases...
In order to assist the design of such innovative devices, the objective of the present project, named ELEC-CELL, is to propose a software for the numerical simulation of dielectrophoresis and electro-deformation of cells under flow, in order to compute the behavior of cells circulating in diagnostic devices while subjected to an electrical field able to displace and deform them.

**Objectives:** 

The main objective of this PhD thesis is to develop a numerical simulation tool to predict the dynamics and deformation of cells in microfluidic systems in which an external electrical field is imposed for medical diagnostics. It means simulating the electro-deformation of cells by proposing an innovative method to solve the equations of electro-hydrodynamics coupled to the equations for the deformation of cellular membranes. The method will be generic, but applied in particular to red blood cells (RBC).
The first application will indeed be to predict the electro-deformation of RBC in blood analyzers that are based on the Coulter principle, which is an electrical impedance measurement. The aim is to enrich the diagnostic capabilities of such blood analyzers by identifying the relationship between impedance signals and biomarkers of the RBC health, namely their shape and mechanical properties. 

**Funding:** 

The funding has been requested at the Doctoral School of the University of Montpellier, but the final answer depends on the candidate. Funding is thus not completely secured, but feedbacks from the institution are very favorable. A serious and motivated candidate would most certainly be selected and funded. The selected candidate will join the YALES2BIO team at IMAG (<https://imag.umontpellier.fr/~yales2bio/>), which has been working for more than 10 years on numerical simulations of blood flows on its industrial applications. 


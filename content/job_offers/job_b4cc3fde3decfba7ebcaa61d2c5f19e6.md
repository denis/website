Title:   Programmation multi-architectures pour la reconstruction à haute performance en tomographie passive
Date: 2021-04-19 10:19
Slug: job_b4cc3fde3decfba7ebcaa61d2c5f19e6
Category: job
Authors: Hamza Chouh
Email: hamza.chouh@cea.fr
Job_Type: Thèse
Tags: these
Template: job_offer
Job_Location: Saclay
Job_Duration: 3 ans
Job_Website: http://www-instn.cea.fr/formations/formation-par-la-recherche/doctorat/liste-des-sujets-de-these/programmation-multi-architectures-pour-la-reconstruction-a-haute-performance-en-tomographie-passive,.html
Job_Employer: CEA-List/Université Paris Saclay
Expiration_Date: 2021-07-31
Attachment: job_b4cc3fde3decfba7ebcaa61d2c5f19e6_attachment.pdf

Le Structural Health Monitoring (SHM, ou contrôle santé intégré) est une approche du contrôle non destructif visant à intégrer des outils d’inspection directement dans les structures ciblées afin de faciliter l’acquisition de données et éviter la mobilisation régulière de ressources de contrôle (humaines, matérielles) et l’immobilisation d’équipements. S’inscrivant dans cette démarche, la tomographie ultrasonore passive exploite le bruit de structure de pièces assimilables à des guides d’ondes pour contrôler les variations d’épaisseur de celle-ci, dans l’objectif de détecter l’apparition de défauts de corrosion ou d’érosion. Ce procédé implique l’utilisation de plusieurs algorithmes de traitement du signal appliqués à de grandes quantités de données. Dans l’objectif d’intégrer des contrôles de SHM dans des équipements à la fois compacts et peu énergivores, cette thèse vise à développer une chaîne de traitement du signal embarquée répondant aux besoins de la tomographie passive. Il sera donc nécessaire de déterminer les architectures matérielles les plus adaptées et de réaliser des implémentations hautement optimisées des algorithmes impliqués dans la chaîne de traitement en faisant évoluer ceux-ci en fonction des besoins de performance. A cette fin on étudiera au cours de la thèse différentes architectures matérielles (GPU, GPU faible consommation, FPGA) en comparant des approches de programmation générique (Sycl) et des implémentations dédiées à chaque architecture.

La thèse se déroulera au sein du Département d&#39;Imagerie et de Simulation pour le Contrôle (DISC) du CEA-LIST et du Laboratoire Siganaux et Systèmes de l&#39;Université Paris Saclay, sous la direction de Nicolas GAC (L2S) et sera co-encadrée par Hamza CHOUH (DISC).
Title: Simulation of Wave Propagation in Highly Heterogeneous Media
Date: 2021-05-08 09:41
Slug: job_f5b1f3548aec2fc6535eafc9a91eecc9
Category: job
Authors: bernacki
Email: marc.bernacki@mines-paristech.fr
Job_Type: Thèse
Tags: these
Template: job_offer
Job_Location: Sophia Antipolis
Job_Duration: 3 ans
Job_Website: 
Job_Employer: MINES ParisTech
Expiration_Date: 2021-06-15
Attachment: job_f5b1f3548aec2fc6535eafc9a91eecc9_attachment.pdf

The objective of the project is thus to develop a high-order FE method for the equations of
elastodynamics, that integrates in the software environment developed at DIGIMU consortium for the numerical simulation of polycrystalline microstructures. A particular attention will
be paid to the efficiency of the parallel implementation on distributed memory architectures (MPI or hybrid OpenMP/MPI techniques). The method will be verified on academic test cases and applied primarily to ultrasonic wave propagation cases in metallic microstructures. Experimental data will be provided by the Aubert &amp;Duval company. More details are summarized in the attachment.


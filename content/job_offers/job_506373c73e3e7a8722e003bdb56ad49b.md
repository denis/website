Title: Administrateur des systèmes d&#39;information de l&#39;IDRIS (H/F) - NOEMI - IE
Date: 2020-12-09 12:10
Slug: job_506373c73e3e7a8722e003bdb56ad49b
Category: job
Authors: Geneviève Morvan
Email: communication@idris.fr
Job_Type: Concours
Tags: concours
Template: job_offer
Job_Location: Orsay (91)
Job_Duration: 
Job_Website: http://www.idris.fr/annonces/idris-recrute.html
Job_Employer: IDRIS-CNRS
Expiration_Date: 2021-01-15
Attachment: 

L&#39;IDRIS recrute, dans le cadre de la campagne de mobilité interne NOEMI-FSEP, ouverte jusqu&#39;au 15 janvier 2021 inclus,

**un(e) ingénieur(e) d&#39;études, administrateur(trice) des systèmes d&#39;information**  (fonction S61016).

Sa mission, au sein de l&#39;équipe système-exploitation, sera de participer au déploiement, à la mise en œuvre et à l&#39;administration de services associés à l&#39;architecture matérielle et logicielle du supercalculateur Jean Zay et des autres serveurs du centre de calcul.

Pour plus de détails sur le poste et/ou pour postuler :
<https://mobiliteinterne.cnrs.fr/afip/owa/consult.affiche_fonc?code_fonc=S61016&type_fonction=&code_dr=4&code_bap=E&code_corps=&nbjours=&page=1&colonne_triee=1&type_tri=ASC>
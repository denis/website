Title: Cluster dynamics simulations of materials under irradiation
Date: 2019-12-17 14:36
Slug: job_be8799afdecceace3edaf2b04dfe95d6
Category: job
Authors: Manuel Athènes
Email: manuel.athenes@cea.fr
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: CEA Saclay
Job_Duration: 12 mois 
Job_Website: https://www.universite-paris-saclay.fr/fr/recherche/laboratoire/service-de-recherches-de-metallurgie-physique-srmp-0
Job_Employer: CEA
Expiration_Date: 2020-03-10
Attachment: 

A postdoctoral position in scientific computation is open in at CEA Saclay (Service de Recherches de Métallurgie Physique, University of Paris-Saclay) in collaboration with EDF R\&amp;D Les Renardières (Département Matériaux et Mécanique des Composants).

**Topics:** Cluster dynamics simulations of materials under irradiation; improvement and parallelization of the solvers serving to integrate the large set of ordinary differential equations (ODE) associated with the involved chemical reactions. 

**Subject:** Alloys used in nuclear applications are subjected to neutron irradiation, which introduces large amounts of vacancy and interstitial defects. Over time, these defects migrate, recombine or agglomerate with minor alloying elements to form small clusters. This affects the physical properties of  the various steels constituting reactor components and their time evolution is to be predicted though multiscale modeling. In this context, the evolution of the microstructure is simulated using the rate equation cluster dynamics method. However, this approach becomes ineffective when several minor alloying elements need being taken into account. Difficulties come from the huge number of cluster variables to deal with and from the stiffness of the ODE set. The latter issue entails that the spectrum of the associated Jacobian matrix is broad.  
The first aspect of the work is theoretical. It involves better understanding the spectral properties of the Jacobian matrix and reformulating the root-finding problem by taking advantage of the reversibility of most chemical reactions. This approach should facilitate the implementation of direct and iterative solvers for  sparse symmetric definite positive matrices, such as the multi-frontal Cholesky factorization and the conjugate gradient methods, respectively. An avenue of research will consist in combining direct linear solvers and iterative solvers, using the former ones as preconditioners of the latter ones. A second aspect of the work aims at optimizing the efficiency of the linear solvers on a distributed parallel architecture within SUNDIALS, the software suite based on which the cluster dynamics code CRESCENDO has been developed. The task is to implement the available parallelized vector and matrix functions from SUNDIALS and to assess their performance. 

Some papers in relation to the proposed work:

T. Jourdan, et al., Efficient simulation of kinetics of radiation induced defects: A cluster dynamics approach, Journal of Nuclear Materials, 444, 298-313, (2014)
P. Terrier, et al., Cluster dynamics modelling of materials: A new hybrid deterministic/stochastic coupling approach, Journal of Computational Physics, 350, p. 280-295, (2017)
M. Athènes et al., Elastodiffusion and cluster mobilities using kinetic Monte Carlo simulations: Fast first-passage algorithms for reversible diffusion processes, Phys. Rev. Materials, 103802 (2019)
P. Amestoy et al., A Fully Asynchronous Multifrontal Solver Using Distributed Dynamic Scheduling, SIAM J. Matrix Analysis and Applications, 23, 15-41 (2001) 

**Profile:** The candidate is expected to hold a PhD in a relevant area of computational physics or applied mathematics since no more than 2 years. He/she will ideally have prior experience in high performance computation and possibly code developments on distributed parallel architectures. Backgrounds in linear algebra and numerical analysis will be an asset.

Title: Post-doc / ingénieur de recherche : Détection et reconnaissance de petits objets dans des images
Date: 2020-01-06 10:35
Slug: job_05cbac5f3e12af2dfdf95b5a5520913f
Category: job
Authors: Baussard Alexandre
Email: alexandre.baussard@utt.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Brest
Job_Duration: 10 à 12 mois
Job_Website: 
Job_Employer: ENSTA Bretagne
Expiration_Date: 2020-02-28
Attachment: 

Le projet DEEPDETECT a été sélectionné par le programme ANR ASTRID financé par la DGA en 2018. Il a pour objectif le développement des méthodes pour résoudre les tâches de détection et de reconnaissance d’objets de petite taille dans des images. Deux applications sont particulièrement considérées : la détection et reconnaissance d’objets (véhicules) dans des images infrarouges ou la détection et la cartographie des populations de mammifères marins par imagerie satellitaire. Typiquement les éléments recherchés ont des tailles de 5x5 à 10x10 pixels dans les images. Mais d’autres contraintes sont aussi à noter : bases de données de tailles limitées, grande variété des fonds, masquages... Pour répondre à la problématique, une première phase du projet a consisté à étudier les solutions existantes, sélectionner les plus pertinentes et à les mettre en œuvre. Différents algorithmes basés sur des réseaux de neurones convolutifs ont notamment été mis en œuvre. 
Dans le cadre de la deuxième phase de ce projet, pour lequel ce recrutement est ouvert, nous allons poursuivre ces travaux et nous intéresser à des problématiques plus spécifiques aux applications considérées comme le transfert d’apprentissage. On s’intéressera particulièrement aux problèmes liés à l’apprentissage à partir de données de synthèse et à la validation sur données réelles en contexte opérationnel.

Dans le cadre de ce projet, la personne recrutée pourra éventuellement se rendre à Paris (1 à 2 mois) dans les locaux de l’entreprise partenaire de ce projet afin de mettre en œuvre les solutions apportées, les tester et évaluer leur robustesse face à des situations opérationnelles.


ATTENTION : Le(la) candidat(e) devra être de nationalité Française ou européenne.
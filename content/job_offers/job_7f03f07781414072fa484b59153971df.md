Title: Chef de projet / Expert en calcul scientifique
Date: 2020-06-09 06:57
Slug: job_7f03f07781414072fa484b59153971df
Category: job
Authors: Maude Le Jeune
Email: lejeune@apc.in2p3.fr
Job_Type: Concours
Tags: concours
Template: job_offer
Job_Location: Paris
Job_Duration: 
Job_Website:  https://www.dgdr.cnrs.fr/drhita/concoursita/
Job_Employer: CNRS
Expiration_Date: 2020-09-01
Attachment: 

Le laboratoire AstroParticule et Cosmologie propose un poste permanent d&#39;ingénieur·e de recherche en calcul scientifique au concours externe CNRS (concours 57).

Toutes les informations pour y candidater sont disponibles à cette adresse: <https://www.dgdr.cnrs.fr/drhita/concoursita/>

Le profil complet est consultable à cette adresse: 
<http://www.dgdr.cnrs.fr/drhita/concoursita/consulter/resultats/consulter.htm>

**Mission : 	 **

Au sein du service informatique du laboratoire Astroparticules et Cosmologie (APC), la mission de l&#39;expert-e en calcul scientifique consiste à concevoir, coordonner et participer à la réalisation des développements informatiques pour le traitement des données et la modélisation instrumentale des expériences d&#39;astronomie multi-messager du laboratoire. Dans un premier temps sur le projet LISA, pour lequel il/elle apportera son expertise dans les aspects liés à la
performance des accès aux données et des calculs, en support aux activités de simulation et d&#39;analyse des données. Dans le cadre de la mission LISA, l&#39;ingénieur-e sera en charge de coordonner et de prendre la responsabilité d&#39;une partie des activités de prototypage du Distributed Data Processing Center (DDPC) dont la France est responsable. Il/Elle sera amené-e à évoluer au sein des différents groupes de travail du consortium en assurant un rôle d&#39;animation et de coordination entre l&#39;équipe système, la communauté scientifique et les agences (CNES, ESA). Il/Elle participera également aux développements logiciels des outils communs des collaborations internationales (LISA, CTA, SVOM, ATHENA).

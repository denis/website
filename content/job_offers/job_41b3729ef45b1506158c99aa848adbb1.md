Title: Ingénieur déploiement et exploitation
Date: 2021-02-03 10:29
Slug: job_41b3729ef45b1506158c99aa848adbb1
Category: job
Authors: Gino Marchetti
Email: gino.marchetti@cc.in2p3.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Villeurbanne
Job_Duration: 1 an renouvelable
Job_Website: https://bit.ly/3qoLXEK
Job_Employer: CNRS / CC-IN2P3
Expiration_Date: 2021-04-28
Attachment: 

Nous recherchons un candidat pour un CDD de 1 an renouvelable pour participer au déploiement de l’instance EGI de DIRAC au CC-IN2P3, dans le cadre du projet européen EGI-ACE.

N’hésitez pas à faire passer l’annonce qui se trouve sur le site de recrutement du CNRS à l’adresse suivante :

<https://bit.ly/3qoLXEK>

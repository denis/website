Title: Research Assistant/Associate in software development for CFD applications
Date: 2021-04-17 09:56
Slug: job_35696bcaa74bc3e73ca42ebeed38e152
Category: job
Authors: Leo Mesquita
Email: lc796@cam.ac.uk
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: Cambridge, UK
Job_Duration: 18 months
Job_Website: https://www.jobs.cam.ac.uk/job/29416/
Job_Employer: University of Cambridge
Expiration_Date: 2021-04-30
Attachment: 

Au Hopkinson Laboratory à l&#39;Université de Cambridge nous cherchons un ingénieur-développeur pour travailler sur l’optimisation de notre code de simulation en combustion (actuellement en fortran) pour qu’il soit capable de tourner de manière efficace des simulations exascale massivement en parallèle. Lien de l&#39;offre avec le descriptif complet: <https://www.jobs.cam.ac.uk/job/29416/>

Pour plus de détails veulliez contacter Savvas Gkantonas (<sg834@cam.ac.uk>) our Léo Mesquita (<lc796@cam.ac.uk>). Contrat de 18 mois et deadline pour postuler 30/04/2021.

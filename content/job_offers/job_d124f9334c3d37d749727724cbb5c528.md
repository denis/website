Title: Caractérisation et modélisation de la microstructure 3D des superalliages à base nickel
Date: 2019-08-22 08:29
Slug: job_d124f9334c3d37d749727724cbb5c528
Category: job
Authors: Bernacki
Email: marc.bernacki@mines-paristech.fr
Job_Type: Thèse
Tags: these
Template: job_offer
Job_Location: Sophia Antipolis
Job_Duration: 3 ans
Job_Website: 
Job_Employer: MINES ParisTech
Expiration_Date: 2020-06-01
Attachment: job_d124f9334c3d37d749727724cbb5c528_attachment.pdf

Contexte du projet:
La chaire industrielle TOPAZE, co-financée par l’ANR et le groupe SAFRAN, porte sur la maîtrise des microstructures et propriétés mécaniques des superalliages à base nickel employés dans les moteurs d’avion et d’hélicoptère de nouvelle génération. Ces matériaux sont employés pour la fabrication de pièces de turboréacteurs en raison de leur tenue mécanique à haute température. L’amélioration des performances de ces alliages permettra d’élever la température de fonctionnement des moteurs et d’en améliorer le rendement, contribuant ainsi à la réduction du coût énergétique et de l’impact écologique du transport aérien. Ce projet fait suite à la chaire industrielle ANR-SAFRAN OPALE qui s’est déroulée entre 2014 et 2019 (chaire opale.cemef.mines-paristech.fr). Il réunit les compétences du CEMEF (MINES ParisTech, UMR CNRS 7635) portant sur l’influence du procédé de mise en forme sur la microstructure
et celles de l’Institut P’ (ISAE-ENSMA, UPR CNRS 3346) sur l’impact de la microstructure sur les propriétés mécaniques en service. Huit doctorants seront recrutés sur la période 2019-2023 pour aborder des aspects complémentaires. La chaire TOPAZE offre ainsi un cadre de travail collaboratif particulièrement riche.

Travaus de thèse:
La caractérisation tridimensionnelle (3D) de la microstructure des superalliages à base nickel fournit des informations essentielles pour l’analyse et la modélisation des mécanismes métallurgiques, ainsi que des modes de déformation et d’endommagement siégeant au sein de ces matériaux. Cependant les méthodes de mesure ou reconstruction de microstructures représentatives 3D sont encore récentes et nécessitent des développements spécifiques. Cette thèse vise à produire des jeux de données expérimentales en 3 dimensions représentant les différents attributs microstructuraux des superalliages à base nickel à différentes échelles :

• L’échelle de la microstructure polycristalline, i.e. l’agrégat de grains, qui doit être constituée d’un nombre de grains représentatif (typiquement plusieurs milliers) dont la taille peut varier de quelques micromètres à quelques centaines de micromètres. Par ailleurs, celle-ci est caractérisée par une présence importante de macles thermiques. Des analyses à l’échelle intragranulaire pourront être conduites pour évaluer l’écrouissage résiduel issu de la mise en forme ou d’une déformation en service.
• L’échelle de la microstructure de précipitation qui est le plus souvent multimodale, avec des tailles de quelques dizaines de nanomètres à quelques microns, ainsi qu’une répartition spatiale
non aléatoire.

Ce travail s’appuiera sur les moyens expérimentaux de microscopie 3D acquis récemment au CEMEF ainsi que sur les codes de simulations numériques de genèse de microstructures développés au CEMEF(chaire-digimu.cemef.mines-paristech.fr). Le comportement mécanique
des microstructures reconstruites sera ensuite évalué au moyen des simulations numériques par éléments-finis en plasticité cristalline en s&#39;appuyant sur les travaux de thèse de Marco Panella (Institut P’, en cours) et les formulations existantes au CEMEF. Par ailleurs les outils numériques 3D de modélisation d’évolutions de microstructures (ReX, GG) seront mis à l’épreuve des données 3D obtenues.
Title: Manager de projet
Date: 2020-11-25 10:08
Slug: job_8fbb793bb1a44da8bcbf338de4574fe3
Category: job
Authors: Delphine Schmitt
Email: delphine.schmitt@math.unistra.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Institut de Recherche Mathématique Avancée (IRMA), Strasbourg
Job_Duration: 12 mois
Job_Website: http://www.unistra.fr/index.php?id=19452&amp;tx_ttnews%5BbackPid%5D=19450&amp;tx_ttnews%5Btt_news%5D=20281&amp;cHash=9a6abbc17e308264d8abeae698726317
Job_Employer: Université de Strasbourg
Expiration_Date: 2021-02-17
Attachment: job_8fbb793bb1a44da8bcbf338de4574fe3_attachment.pdf

L&#39;Institut Thématique Interdisciplinaire (ITI) IRMIA++ va prendre le relais et amplifier les actions du projet de Labex IRMIA, qui touche à sa fin.
La personne recrutée assurera la coordination opérationnelle du projet auprès du coordinateur de l’ITI, des responsables d’axes de développement du projet, et plus largement en appui aux organes de gouvernance de l’ITI (comité exécutif, comité de pilotage, comité d’orientation). La première année, elle veillera au bon déroulement de la transition de la structure LabEx vers la structure ITI.

Le manager de projet sera ainsi le garant de la bonne application de la charte ITI, dans toutes ses dimensions.
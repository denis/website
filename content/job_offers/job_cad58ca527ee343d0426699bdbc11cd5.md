Title: Ingénieur de recherche CNRS expert en calcul scientifique (Physique/Chimie théorique)
Date: 2021-01-08 10:53
Slug: job_cad58ca527ee343d0426699bdbc11cd5
Category: job
Authors: Marie-Bernadette Lepetit
Email: Marie-Bernadette.Lepetit@neel.cnrs.fr
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: Grenoble
Job_Duration: 
Job_Website: 
Job_Employer: CNRS
Expiration_Date: 2021-04-02
Attachment: job_cad58ca527ee343d0426699bdbc11cd5_attachment.pdf

**Corps** : Ingénieur de recherche
**BAP** : E-Informatique, statistiques et calcul scientifique
**Emploi-type** : Expert-e en calcul scientifique
**Quotité** : Temps complet, poste mutualisé LPMMC/Néel,  (forme : mutation FSEP)

**Description des missions** :
L&#39;ingénieur.e de recherche pilote la partie technique des activités de calcul scientifique et d&#39;optimisation de simulations numériques pour la physique de la matière condensée de l’Institut Néel (60 % du temps) et du Laboratoire de Physique et Modélisation des Milieux Condensés (pour 40% du temps). 

**Activités principales**

* Participer, avec les chercheurs , à la conception et au développement de codes de simulation pour la physique de la matière condensée (notamment simulations ab initio et spectroscopie ), la physique quantique et la physique statistique.
* Évaluer la qualité et l&#39;efficacité des codes développés ou existants
* Proposer des solutions techniques (choix d&#39;algorithme, optimisation de la compilation, parallélisation…) pour pousser ces codes à l&#39;état de l&#39;art
* Porter les codes sur d&#39;autres plateformes en fonction de l&#39;architecture des machines de calcul ciblées
* Développer des interfaces utilisateurs dans des langages de script, permettant la diffusion de codes à la communauté internationale et contribuer au rayonnement des recherches menées dans les laboratoires partenaires
* Assurer le suivi vis à vis des utilisateurs de codes existants ou en développement
* Assurer la veille technologique vis à vis des nouvelles méthodes et plateformes (par ex. machine learning et tensorflow)

**Activités associées**

* Participer à des projets de recherche au plan national et international et aux publications associées
* Participer à la valorisation des travaux
* Assurer le transfert des connaissances, des savoirs-faire, et des bonnes pratiques auprès des chercheurs : participer à la formation des utilisateurs du calcul numérique intensif, diffuser et valoriser les méthodes et outils développés.
* Interlocuteur avec les structures de calcul intensif, notamment GRICAD, et grands instruments (GENCI, ESRF,SOLEIL, etc.)

Title: Ingénieur·e Biostatisticien·ne H/F
Date: 2020-02-14 15:56
Slug: job_20d79456f3e6be73c62a966a00ad77f5
Category: job
Authors: Cordero
Email: apply-4485435c81ad01@adoc-tm.breezy-mail.com
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Paris 12eme
Job_Duration: 1 an renouvelable
Job_Website: https://adoc-tm.breezy.hr/p/4485435c81ad01
Job_Employer: Adoc Talent Management
Expiration_Date: 2020-05-08
Attachment: 

Adoc Talent Management recrute pour son client un·e Ingénieur·e Biostaticien·ne H/F. Notre client est un centre de gestion et de méthodes en appui aux études cliniques d’un institut fédérant les acteurs académiques majeurs en épidémiologie et santé publique. Il a pour objectif principal le développement des connaissances sur plusieurs problèmes de santé publique prioritaires. Au service de la communauté scientifique et des agences et autorités de santé, les équipes mettent en place les méthodes les plus innovantes pour assurer un très haut niveau de résultat et exploiter au mieux une importante quantité de données de santé sur une cohorte de très grande taille.

Poste

Au sein de l’équipe dédiée à l’« épidémiologie clinique des maladies virales chroniques », votre activité sera consacrée aux projets utilisant les données du Système National des Données de Santé (SNDS), notamment liées aux études de cohorte conduites par le centre. Vous définirez et mettrez en œuvre les analyses statistiques de ces projets et des bases de données associées.

Pour ce faire, vous appliquerez les méthodes statistiques existantes les plus pertinentes afin de collecter et analyser les données puis en interpréter les résultats, ainsi que la contribution à la diffusion et la valorisation des études.

Également, vous développerez des programmes de modélisation propres aux études statistiques et rendrez accessible la documentation nécessaire aux utilisateurs de la chaîne de traitement des données.

Vous participerez au reporting des résultats des analyses statistiques et communiquerez sur l’avancée des activités auprès des acteurs internes. Enfin, dans un objectif de transfert de compétences, vous serez amené·e à intervenir sur certaines formations et assurerez une veille scientifique dans votre domaine de compétences, impliquant votre participation à des congrès nationaux ou internationaux. Vous pourrez également, si vous le souhaitez, gérer des projets d’analyses statistiques pour des partenaires académiques et du secteur pharmaceutique.

Le poste est à pourvoir au plus vite, en CDD, à Paris avec de fortes perspectives d’évolution en CDI ou vers une titularisation dans la fonction publique.

Profil

Vous êtes titulaire d’un diplôme d’ingénieur, Master 2 ou doctorat en statistiques, biostatistiques ou épidémiologie. Vous maitrisez parfaitement le logiciel SAS et vous avez une connaissance au moins théorique (au mieux pratique) du SNDS. Vous avez dans l’idéal une connaissance des maladies virales chroniques (VIH, VHC, VHB…) et avez une expérience significative en construction et gestion de bases de données dans le cadre d’études statistiques.

Vous faites preuve d’un réel esprit d’analyse et avez l’habitude de travailler sur des projets en équipe pluridisciplinaire. Votre rigueur et votre méthodologie de travail vous permet une bonne gestion des priorités. Vous êtes motivé·e à vous former sur de nouvelles méthodes.

Si l’idée de baigner dans un environnement dédié à la recherche et l’amélioration de la santé de premier plan vous motive, envoyez-nous votre candidature sans plus tarder à Adoc Talent Management.
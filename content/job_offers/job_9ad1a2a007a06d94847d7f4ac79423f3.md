Title: Ingénieur-e de recherche CNRS en calcul scientifique
Date: 2021-06-10 10:51
Slug: job_9ad1a2a007a06d94847d7f4ac79423f3
Category: job
Authors: Céline PARZANI
Email: celine.parzani@tse-fr.eu
Job_Type: Concours
Tags: concours
Template: job_offer
Job_Location: Toulouse laboratoire TSE-R
Job_Duration: 
Job_Website: https://www.tse-fr.eu/fr
Job_Employer: CNRS 
Expiration_Date: 2021-09-02
Attachment: job_9ad1a2a007a06d94847d7f4ac79423f3_attachment.pdf

Un poste d&#39;ingénieur-e de recherche en calcul scientifique est ouvert au concours externe par le CNRS. L&#39;ingénieur-e travaillera au sein de l’équipe calcul scientifique de l’unité TSE-R (Toulouse Scool of Economy).
Il/Elle apportera son expertise en matière d’optimisation de l’usage intensif de grandes bases de données tant sur le plan du stockage que sur le plan de l’exploitation sur les architectures disponibles dans (ou hors de) l’établissement.
En réponse au besoin des utilisateurs et en lien avec l’infrastructure de calcul scientifique, il/elle mettra en place les architectures matérielles et logicielles appropriées pour le stockage et la bonne utilisation des données au sein de l’unité.

Le profil est précisé dans le fichier joint ou bien ici <http://www.dgdr.cnrs.fr/drhita/concoursita/consulter/resultats/consulter.htm> concours n°55, 2ème poste. 
Pour plus d&#39;information, contacter Céline Parzani (<celine.parzani@tse-fr.eu>).
Informations pour candidater: <http://www.dgdr.cnrs.fr/drhita/concoursita/>
Date limite de dépôt de candidatures: 30 juin 2021 à 13h (heure de Paris).

Title: Ingénieur développeur logiciel CFD
Date: 2021-02-03 08:28
Slug: job_a23e52b5832d3191f0b3b4b31d85e78f
Category: job
Authors: Denis Gueyffier
Email: denis.gueyffier@onera.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Châtillon - région parisienne
Job_Duration: 18 mois
Job_Website: http://www.onera.fr
Job_Employer: ONERA
Expiration_Date: 2021-04-28
Attachment: 

INGENIEUR DEVELOPPEUR LOGICIEL CFD 18 MOIS H/F

Présentation ONERA

L’ONERA, acteur central de la recherche aéronautique et spatiale, emploie environ 1950 personnes. Placé sous la tutelle du Ministère des Armées, il dispose d’un budget de 236 millions d’euros dont plus de la moitié provient de contrats commerciaux. Expert étatique, l’ONERA prépare la défense de demain, répond aux enjeux aéronautiques et spatiaux du futur, et contribue à la compétitivité de l’industrie aérospatiale. Il maîtrise toutes les disciplines et technologies du domaine.

Présentation du département

Le Département Aérodynamique, Aéroélasticité, Acoustique, DAAA de l’ONERA, a pour mission de préparer au profit de l’industrie des réponses technologiques pour améliorer les performances Aérodynamique, Aéroélastique et Acoustique des aéronefs, et répondre aux enjeux de compétitivité, aux besoins sociétaux, environnementaux et de défense.

Missions

Vous participez aux travaux de l&#39;unité Conception et production de Logiciels pour les Ecoulements de Fluides (CLEF) du DAAA. La mission de cette unité est de réaliser des grandes plateformes logicielles de simulation en Mécanique des Fluides, aptes à répondre aux besoins de recherche et d&#39;applications de l&#39;ONERA, de laboratoires de recherche partenaires et des utilisateurs de l&#39;industrie.

Dans cette unité, vous prendrez en charge des travaux au sein du logiciel de simulation en aérodynamique elsA ou de son successeur. Un enjeu majeur de la production d’un solveur moderne réside dans l’obtention du code différentié. Ce dernier est en particulier nécessaire pour l’algèbre linéaire (résolution de systèmes, préconditionnement, recherche de valeurs propres, …). En vue de produire ce code différentié, vous devrez régulariser certains schémas et fonctionnalités avant d’utiliser un outil de différentiation algorithmique. Vous serez également en charge de la définition, de la mise en œuvre et de l’utilisation d’une stratégie de vérification par différences finies et/ou tests de dualité des codes linéarisé et adjoint produits. Un des objectifs est de pouvoir automatiser ces vérifications et de pouvoir les réaliser à différentes granularités. Vous conduirez également une réflexion sur la validation du code différentié par une approche en algèbre complexe. Le travail comprendra donc des travaux d’étude bibliographique, de recherche, de développements au sein des solveurs ou dans leur chaine logicielle ainsi que de validation des méthodes depuis des cas tests simplifiés jusqu’à des cas de complexité opérationnelle.

Profil

- Docteur et/ou ingénieur avec spécialisation en simulation numérique
- Goût pour la recherche (méthodes numériques, développement algorithmique, algèbre linéaire, modélisation physique…)
- Expérience réussie en développement de logiciels de calcul scientifique en environnement HPC de préférence dans le domaine de la CFD
- Compétences techniques requises avérées : Fortran (&gt;=90), C++ (&gt;=11), MPI, Python, Numpy
- Compétences techniques souhaitées : méthode adjointe, différentiation algorithmique, CGNS, Git, Subversion.

Intégrer l’ONERA, c’est rejoindre le premier acteur de la recherche aéronautique et spatiale en France, c’est construire le futur dès à présent, c’est innover chaque jour dans un secteur de pointe.
À l’ONERA, vous trouverez un équilibre vie professionnelle/vie personnelle et un environnement favorisant la formation continue des collaborateurs.

Postuler sur :
<https://www.onera.fr/fr/rejoindre-onera/offres-emploi/detail?jobId=1303&amp;jobTitle=INGENIEUR%20DEVELOPPEUR%20LOGICIEL%20CFD%2018%20MOIS%20H%2FF>

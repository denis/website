Title: Expert-e  en Calcul Scientifique
Date: 2020-03-12 11:42
Slug: job_9013d76d212e91c636002a224899a846
Category: job
Authors: Mondher CHEKKI
Email: mondher.chekki@univ-grenoble-alpes.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Grenoble
Job_Duration: 1 an renouvelable
Job_Website: http://www.ige-grenoble.fr
Job_Employer: UGA
Expiration_Date: 2020-03-29
Attachment: job_9013d76d212e91c636002a224899a846_attachment.pdf

Ce recrutement se fait dans le cadre du projet ANR EIS, piloté par l’IGE, et qui a vocation à favoriser l’utilisation d’Elmer/Ice dans la communauté française. La personne recrutée aura des interactions avec les partenaires du projet qui seront utilisateurs des nouveaux développements (Laboratoire des Sciences du Climat et de L’Environnement (LSCE, Saclay) et
Centre National de la Recherche Météorologiques (CNRM, Toulouse)).
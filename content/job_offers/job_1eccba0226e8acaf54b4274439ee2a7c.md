Title: Ingénieur de recherche &#34;Modélisation de conversion de l’énergie et émulation temps réel&#34;
Date: 2021-01-25 11:55
Slug: job_1eccba0226e8acaf54b4274439ee2a7c
Category: job
Authors: Damour Cédric
Email: cedric.damour@univ-reunion.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Saint-Denis de La Réunion
Job_Duration: 24 mois
Job_Website: https://www.le2p.fr/
Job_Employer: Université de La Réunion
Expiration_Date: 2021-02-20
Attachment: job_1eccba0226e8acaf54b4274439ee2a7c_attachment.pdf

Nous recrutons un ingénieur de recherche en &#34;Modélisation de conversion de l’énergie et émulation temps réel&#34; dans le cadre du projet FEDER DETECT.

Pour plus d&#39;information et pour postuler, voir le fichier joint.

Vous pouvez également nous contacter directement par mail si vous souhaitez plus d&#39;information.

Cédric DAMOUR
MCF HDR au laboratoire Energy-lab (<https://www.le2p.fr/>)
Porteur du projet DETECT

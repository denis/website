Title: Ingénieur de recherche en calcul scientifique - éléments finis, infographie, optimisation pour la biophysique
Date: 2021-01-11 17:36
Slug: job_e228b49a345b6c21490164ec575c4bc1
Category: job
Authors: Hervé Turleir
Email: herve.turlier@college-de-france.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Collège de France, Paris
Job_Duration: 24 mois renouvelable
Job_Website: https://bit.ly/35snVAT
Job_Employer: CNRS
Expiration_Date: 2021-02-11
Attachment: job_e228b49a345b6c21490164ec575c4bc1_attachment.pdf

Le projet ERC DeepEmbryo, porté par Hervé Turlier, comporte une forte composante numérique, qui nécessite le soutien d&#39;un expert en calcul scientifique spécialisé en informatique graphique, modélisation par éléments finis et optimisation numérique. L&#39;ingénieur(e) recruté(e) développera et mettra en oeuvre des algorithmes d&#39;informatique graphique, de modélisation par éléments finis et d&#39;optimisation pour modéliser la mécanique et la dynamique des cellules dans un embryon, en collaboration avec les membres de l&#39;équipe &#34;Physique multi-échelle de la morphogenèse&#34;. Elle/il assurera le maintien des logiciels développés dans l&#39;équipe et la mise en place d&#39;interfaces utilisateurs dédiés. Elle/il assistera la formation des nouveaux arrivants dans l&#39;équipe aux logiciels utilisés dans l&#39;équipe et pourra co-encadrer des étudiants.

L&#39;ingénieur(e) recruté(e) aura la responsabilité :

- de développer de nouveaux algorithmes d&#39;informatique graphique en C++ en 2D et 3D (maillage triangulaire non-manifold)
- d&#39;implémenter la résolution de modèles d&#39;éléments finis en C++
- de développer de nouveaux algorithmes d&#39;optimisation numérique en C++.
- de développer des interfaces utilisateur (GUI) pour les logiciels développés au sein de l&#39;équipe et d&#39;assurer l&#39;interfacage Python des codes C++.
- d&#39;assister les nouveaux entrants dans l&#39;équipe à la prise en main des logiciels et des bonnes pratiques informatiques (git, utilisation du cluster, )
- d&#39;assurer la gestion du cluster informatique de l&#39;équipe, incluant serveurs CPU, GPU et de stockage (mise à jour des logiciels, librairies, gestion des permissions)
- de participer à la formation des membres de l&#39;équipe en programmation et en algorithmique
- de tenir à jour le sites web de la future plateforme numérique
- de participer à la vie scientifique de l&#39;équipe

La/le candidat(e) devra posséder un diplôme d&#39;ingénieur ou un doctorat en informatique, mathématiques appliquées ou physique.
Compétences requises :

- expertise en programmation orientée objet C++ (et Python)
- expertise en modélisation par méthodes d&#39;éléments finis
- solides connaissances en informatique graphique (maillage triangulaire, remaillage adaptatif etc...)
- solides connaissances des méthodes d&#39;optimisation et des bibliothèques C++ d&#39;algèbre linéaire
- connaissance des méthodes de parallélisation sur CPU (OpenMP/MPI) et GPU (CUDA)
- des connaissance en mécanique (coques minces, structures) et en mécanique des fluides seront un avantage majeur
- disponibilité, réactivité et autonomie
- organisation et rigueur dans le travail
- qualités de communication et de pédagogie 
- sens développé du travail en équipe et parfaite maîtrise de l&#39;anglais

La/le candidat(e) retenu(e) sera accueilli(e) au sein de l&#39;équipe interdisciplinaire &#34;Physique multiéchelle de la morphogenèse&#34; dirigée par Hervé Turlier qui comptera à l&#39;horizon 2021 environ 8 à 10 personnes (www.turlierlab.com). L&#39;équipe est hébergée au Centre Interdisciplinaire de Recherche en Biologie au Collège de France, laboratoire CNRS/INSERM/Collège de France situé au coeur du quartier latin à Paris. Intégré au sein de l&#39;Université PSL, et proche d&#39;autres grandes institutions comme l&#39;Ecole Normale Supérieure et l&#39;Institut Curie, le Collège de France constitue un environnement scientifique exceptionnel et unique au monde.
La/le candidat(e) retenu(e) aura accès

- à un poste de travail individuel dans des locaux rénovés
- à un puissant ordinateur fixe ou portable
- à un cluster de calcul haute performance (CPU et GPU).
Si elle/il le souhaite, l&#39;ingénieur(e) sera assisté(e) pour postuler aux concours de la fonction publique pour obtenir un poste permanent d&#39;ingénieur de recherche.

La/le candidat(e) devra envoyer un CV et une lettre de motivation accompagnée au minimum d&#39;une lettre de recommandation via le portail emploi du CNRS: <https://bit.ly/35snVAT>
Tout dossier incomplet ne sera pas considéré.

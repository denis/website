Title: Ingénieur de recherche en IA pour la conception (quantification incertitudes) (CDI) H/F
Date: 2019-12-23 12:44
Slug: job_2fce11b43de9b9d19a9552b7372752a5
Category: job
Authors: Adoc Talent Management
Email: apply-60c878bdc36301@adoc-tm.breezy-mail.com
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: Magny-les-hameaux
Job_Duration: 
Job_Website: https://www.adoc-tm.com/
Job_Employer: Adoc Talent Management
Expiration_Date: 2020-03-16
Attachment: 

Adoc Talent Management recherche un·e ingénieur·e de recherche en IA pour la conception, sur les problématiques de quantification d’incertitudes pour son client, centre de recherche industriel d’un groupe Français présent au niveau international sur le secteur des technologies et équipements aéronautiques.

Poste

Dans le cadre d&#39;un projet pluridisciplinaire amont lancé par le centre de recherche sur l&#39;utilisation de l&#39;IA pour la conception et la simulation, vous travaillerez sur le développement des outils issus de l’IA afin de proposer de nouvelles stratégies d’optimisation de simulations numériques et de représentativité des modèles.

Vous participerez en particulier aux travaux de recherche sur la quantification des incertitudes de prédiction pour les réseaux de neurones profonds, impliquant de travailler dans des espaces de grande dimension, et aurez pour premières pistes les techniques d&#39;échantillonnage récentes, couplées à des méthodes de réduction de dimension. Pour ceci, vous réaliserez tout au long du projet une veille scientifique et technique qui servira à identifier les pistes les plus prometteuses pour traiter des cas en très grande dimension.

Enfin vous participerez au réseau interne d&#39;échanges scientifiques autour de l&#39;IA, et valoriserez les travaux de recherche auprès de tous les pôles du centre de recherche. Vous communiquerez sur les résultats à travers des publications scientifiques et la participation à des congrès et conférences internationales.

Profil

Titulaire d’un diplôme d’ingénieur ou de doctorat, vous avez environ 3 ans d’expérience professionnelle sur des projets de recherche nécessitant une expertise mathématique et l’utilisation d’outils statistiques, en particulier les méthodes bayésiennes.

Vous avez également une expertise en échantillonnage et réduction de dimension, et une grande curiosité pour d’autres domaines et exploiter le potentiel des outils d’IA.

A travers la gestion de vos projets, vous avez pu développer une bonne capacité de communication ainsi qu’une grande autonomie, tout en ayant un fort esprit d’échange et de partage.

Si vous souhaitez vous investir au sein d’une structure Française ayant su faire sa place au niveau international, sur un poste vous permettant autonomie et forte interactions humaines, envoyez-nous votre candidature \!
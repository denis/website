Title: Real-time deterministic wave prediction applied to floating wind turbines
Date: 2021-04-06 16:45
Slug: job_78b5b89f2366f56cd2c060c45cd32f40
Category: job
Authors: Ducrozet Guillaume
Email: guillaume.ducrozet@ec-nantes.fr
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: Nantes
Job_Duration: 18 mois (renouvelable)
Job_Website: 
Job_Employer: Centrale Nantes
Expiration_Date: 2021-06-01
Attachment: job_78b5b89f2366f56cd2c060c45cd32f40_attachment.pdf

A postdoctoral position is available at the LHEEA Lab. of Centrale Nantes (France). The postdoctoral researcher will work on the development of a numerical tool to predict in real-time the wave field from a set of measurements. The final objective is to use the algorithm in operating conditions on floating offshore wind turbines. To this end, it needs to operate faster than physical time (allowing control strategies of wind turbines for instance). This position is available within the framework of two funded projects: EU (H2020) FLOATECH and French (ANR) CREATIF. Candidates with experience in scientific computing/numerical analysis and interest in ocean engineering are encouraged to apply.  Applicants must have a Ph.D. in Applied Mathematics or relevant field of engineering such as Fluid Dynamics, Inverse Problems, Signal Processing, etc. as well as strong quantitative and writing skills.

The initial appointment is for 18 months, with the possibility of renewal. Details are available in the attached document.

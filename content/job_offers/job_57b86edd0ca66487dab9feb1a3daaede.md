Title: Ingénieur de Recherche numéricien - Modélisation procédés en milieux réactifs ou bio-actifs 
Date: 2021-01-12 14:39
Slug: job_57b86edd0ca66487dab9feb1a3daaede
Category: job
Authors: Patrick Perré
Email: patrick.perre@centralesupelec.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Pomacle (51) 
Job_Duration: 36 mois 
Job_Website: http://www.chaire-biotechnologie.centralesupelec.fr/
Job_Employer: CentraleSUpélec 
Expiration_Date: 2021-03-12
Attachment: job_57b86edd0ca66487dab9feb1a3daaede_attachment.pdf

**Ingénieur de recherche numéricien**  
**Research engineer in computational science**  
*CDD de 3 ans renouvelable*


Poste ouvert au sein de la Chaire de Biotechnologie de CentraleSupélec, localisée dans le Centre Européen de Biotechnologie et de Bioéconomie (CEBB), à Pomacle (20 km de Reims).

**ENVIRONNEMENT** :  
La Chaire de Biotechnologie de CentraleSupélec, créée en 2011 et localisée au sein de la bioraffinerie de Bazancourt-Pomacle (51), est structurée autour de trois axes thématiques : i) lignocellulosiques, ii) bio transformation et iii) techniques séparatives, le tout s’appuyant sur un socle transversal modélisation, instrumentation &amp; visualisation. Il s’agit de l’un des quatre groupes hébergés par le Centre Européen de Biotechnologie et de Bioéconomie (CEBB). 
La Chaire de Biotechnologie de CentraleSupélec est adossée au Laboratoire de Génie des Procédés et Matériaux (LGPM) localisé à Gif-sur-Yvette (91).
La Chaire de Biotechnologie de CentraleSupélec a été renouvelée en 2020 . Ceci permet une montée en puissance substantielle, avec l&#39;ambition de l&#39;utilisation massive de la modélisation couplée à la micro-expérimentation pour passer à l&#39;échelle industrielle grâce au concept de jumeau numérique de la bioraffinerie.  La halle technique du CEBB héberge un ensemble de pilotes de laboratoire propice à la validation de cette approche virtuelle. La montée en puissance se traduira notamment par plusieurs recrutements, par l&#39;acquisition d&#39;équipements scientifiques remarquables et l’investissement dans des moyens de calcul mutualisés. 

**MISSIONS** :  
En collaboration avec les équipes scientifiques du LGPM, l’ingénieur de recherche numéricien participera au développement du projet de jumeau numérique dans le domaine d’expertise des bioprocédés. Pour cela, il/elle participera au développement de nouveaux codes numériques dans le domaine de la modélisation des procédés en milieux réactifs ou bio-actifs (transferts couplés, modélisation 3D sur morphologie évolutive) et au déploiement de ces outils sur les architectures de calcul (mésocentres ROMEO à Reims, et FUSION à CentraleSupélec). Ses compétences en analyse numérique lui permettront de résoudre les formulations par les méthodes classiques d&#39;intégration des équations aux dérivées partielles (FEM, CVM) et/ou par méthodes discrètes (LBM, automates cellulaires). L&#39;équipe dispose d&#39;un nanotomographe permettant de décrire les morphologies 3D et leur évolution induite par activité réactive (par exemple pyrolyse de biomasse) ou biologique (par exemple développement de biofilm en milieux poreux).

**COMPÉTENCES** :  
Titulaire d’un diplôme d’ingénieur ou d’un doctorat ou niveau équivalent attesté par une expérience professionnelle, les candidats devront : 
-	Avoir de bonnes connaissances en mathématiques appliquées (analyse numérique),
-	Maitriser au moins un langage de programmation scientifique (Fortran, C/C++, Matlab, R, Python...) et les méthodes de génie logiciel, 
-	Avoir des bonnes aptitudes au travail collaboratif,
-	Avoir une bonne maîtrise de l&#39;anglais,
-	Avoir un intérêt voire des connaissances dans les champs disciplinaires et applicatifs de la Chaire.

**MODALITÉS PRATIQUES** :  
Le poste est ouvert au sein du Centre Européen de Biotechnologie et de Bioéconomie (CEBB), qui héberge la Chaire de Biotechnologie : CEBB – 3, rue des Rouges Terres 51110 Pomacle
Des déplacements sur le site de CentraleSupélec à Gif-sur-Yvette seront à prévoir.
Le salaire sera déterminé en fonction de l’expérience du candidat.

**DOCUMENTS À FOURNIR** :  
Les lettres de candidature, accompagnées d’un curriculum vitae et, à la discrétion des candidats, de lettres de recommandation, devront être adressées par courriel uniquement aux deux contacts mentionnés ci-après.

**CONTACT** :  
 
Prof. Patrick PERRÉ,  
Directeur de la Chaire de Biotechnologie,  
LGPM, CentraleSupélec  
patrick.perre@centralesupelec.fr  
Tél. : + 33 6 42 61 24 18  

Victor POZZOBON  
Ingénieur de Recherche, HDR  
LGPM, CentraleSupélec  
Victor.pozzobon@centralesupelec.fr  
Tél. : +33 3 52 62 05 08  

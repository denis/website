Title: Post-doctoral researcher position : Numerical modeling of permafrost thermo-hydrological dynamics with OpenFOAM® using High Performance Computing  in the framework of the HiPerBorea project
Date: 2020-03-13 09:26
Slug: job_74dfe9dd80d90aa23ee1216ebc0d3dd1
Category: job
Authors: Laurent Orgogozo
Email: laurent.orgogozo@get.omp.eu
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: France, Toulouse, Observatoire Midi-Pyrénées, Laboratoire Géosciences Environnement Toulouse
Job_Duration: 24 months (+ a possible extension of 12 months)
Job_Website: https://www.researchgate.net/project/HiPerBorea-High-Performance-computing-for-quantifying-climate-change-impacts-on-Boreal-Areas-ANR-funded-project-ANR-19-CE46-0003-01/update/5e68aadd3843b0499feec1a1
Job_Employer: CNRS
Expiration_Date: 2020-10-01
Attachment: job_74dfe9dd80d90aa23ee1216ebc0d3dd1_attachment.pdf

Post-doctoral researcher position :
Numerical modeling of permafrost thermo-hydrological dynamics with OpenFOAM® using High Performance Computing
 in the framework of the HiPerBorea project


Location: GET laboratory (www.get.omp.eu) in Toulouse, France. 
                 Lab. Affiliated to the Observatory Midi-Pyrénées (http://www.omp.eu).

Contract duration: Fixed-term period of 24 months with possibility of an extension, to be started in january 2021.

Salary: 2100 euros/month to 3150 euros/month (all taxes paid, including medical insurance) depending on the number of years after PhD defense, according to the national CNRS directives.

Contact: Laurent Orgogozo, University of Toulouse – Paul Sabatier Toulouse III (laurent.orgogozo@get.omp.eu).

Keywords: numerical simulation, Computational Fluid Dynamics (CFD), OpenFOAM®, High Performance Computing (HPC), supercomputers, massively parallel computing, heat transfer in porous media, flow in porous media, freeze/thaw processes, cryohydrogeology, cryohydrology, permafrost modeling, boreal areas, climate change impacts.

Scientific context:
	This researcher position is associated with the HiPerBorea project funded by the French National Research Agency (ANR). HiPerBorea aims at enabling quantitative and predictive modeling of cold regions hydrosystems evolution under climate change. Arctic and sub-arctic areas, which are highly vulnerable to global warming, are largely covered by permafrost – soil that is year-round frozen at depth. Permafrost-affected areas, which represent 25% of emerged lands of the northern hemisphere, currently experience fast and important changes of both hydrological and thermal processes in response to climate change. In this project, advanced numerical modeling will be used to help predict the impact of permafrost thaw on thermo-hydrological status of the arctic regions. By doing so, HiPerBorea will provide mechanistic understanding of Arctic change, that is necessary to further understand carbon cycle and contaminant/nutrient transport, and to further assess risk and opportunity for sustainable urbanization, agriculture and general sustainable development of the (sub-)Arctic. Specifically, HiPerBorea will focus on producing quantitative estimates of climate change impacts on the thermo-hydrological status of permafrost in four experimental watersheds under long-term environmental monitoring (e.g.: stations of the INTERACT network). Studying the behaviors of these four watersheds which cover a large longitudinal gradient from Scandinavia to Eastern Siberia, we expect to get key information on the response of boreal areas to climate change. 
	The chosen strategy to reach this goal is numerical modeling based on computational fluid dynamics methods applied to cryohydrogeology and cryohydrology. The physical processes to be simulated are hydrogeological/hydrological transfers and thermal transfers with seasonal freeze/thaw cycles on boreal continental surfaces, which are described by strongly coupled and non-linear partial differential equations. Due to the large time scales (up to the century) and space scales (up to several tens of km²) to be dealt with, large computational loads will be encountered and the use of high performance computing will be required. In HiPerBorea the permaFoam solver for cryohydrogeology (Orgogozo et al., 2019) will serve as a basis for the development of a new open source cryohydrological simulator that will allow to perform the considered numerical modelings. The permaFoam solver has been developed in the framework of the well-known OpenFOAM®* computational fluid dynamics open source tool box, mainly in order to benefit from its good capabilities for massively parallel computation (e.g.: Orgogozo et al., 2014). The use of a new permaFoam based cryohydrological simulator on modern supercomputers such as the tier-2 supercomputer Olympe** of CALMIP or the tier-1 supercomputer Occigen*** of the CINES (and even tier-0 European supercomputing infrastructure if needed) will allow to perform the simulations of thermal and hydrological responses of the considered experimental catchments to various scenarios of climate change. These simulations will be unprecedented in terms of scales and resolutions in the field of cryo-hydrogeology and will be made using cutting edge modeling techniques. It is expected that they will give key novel insights on the on-going arctic changes, such as enveloppes for active layer thickness evolutions or for water fluxes evolutions within each considered watershed.
* https://www.openfoam.com/ and https://openfoam.org/ 
** https://www.calmip.univ-toulouse.fr/spip.php?article582
*** https://www.cines.fr/calcul/materiels/occigen/ 

Job description: For each four studied watershed, the hired researcher will run the simulations of thermal and hydrological responses to climate change according to the various CMIP 5 scenarios using the HPC permafrost simulator developed in the HiPerBorea project (already operational flavor: permaFoam, Orgogozo et al., 2019 ; to be extended and developed during the HiPerBorea project by the other partners of the project). He/she will do the conditioning of the modelings (e.g.: building meshes representing Digital Elevation Models, boundary conditions that take into account meteorological forcings and land cover) on the basis of the field data and the satellite data existing for each watershed, using the advanced pre-processing tools of the OpenFOAM® environment (e.g.: snappyHexMesh, swak4foam). Once he/she will have performed the simulations on regional to European supercomputers, he/she will interpret the obtained results to quantitatively assess the potential impacts of climate change on boreal permafrost through watershed-wise analyses and comparative analyses between the considered watersheds. He/she will present the acquired knowledge in international scientific conferences and publish them in peer-reviewed, open access journals with high impact factors.

Required education, experience and skills: The hired researcher must be familiar with computational fluid dynamics methods, and should have expertise in at least one of the additional field listed below:
- transport in porous media ;
- high performance computing ;
- permafrost modeling.
An experience with the use of OpenFOAM would be highly appreciated, although not mandatory.  Team-working state of mind and excellent scientific communication skills are also expected.

How to apply: Applicants should submit a complete application package by email to Laurent Orgogozo (laurent.orgogozo@get.omp.eu). It should include (1) a curriculum vitae including most important recent publications, (2) a statement of motivation and (3) names, addresses, phone numbers, and email addresses of at least two references. The application should be preferably submitted before the 1st of October 2020.

Bibliography: 
Orgogozo L., Prokushkin A.S., Pokrovsky O.S., Grenier C., Quintard M., Viers J., Audry S., 2019. Water and energy transfer modeling in a permafrost-dominated, forested catchment of Central Siberia: the key role of rooting depth. Permafrost and Periglacial Processes 30 : 75-89.
https://hal.archives-ouvertes.fr/hal-02014619 
Grenier C., Anbergen H., Bense V., Chanzy Q., Coon E., Collier N., Costard F., Ferry M.,  Frampton A., Frederick J., Gonçalvès J., Holmén J., Jost A., Kokh S., Kurylyk B., McKenzie J.,  Molson J.,  Mouche E., Orgogozo L., Pannetier R., Rivière A., Roux N., Rühaak W.,  Scheidegger J., Selroos J.-O., Therrien R., Vidstrand P., Voss C., 2018. Groundwater flow and heat transport for systems undergoing freeze-thaw: Intercomparison of numerical simulators for 2D test cases. Adv. Water Resour., 114, 196-218.
https://hal.archives-ouvertes.fr/hal-01396632 
Orgogozo L., 2015. RichardsFOAM2: a new version of RichardsFOAM devoted to the modeling of the vadose zone. Computer Physics Communications 196 : 619-620. DOI: 10.1016/j.cpc.2015.07.009 https://hal.archives-ouvertes.fr/hal-01299854 
Orgogozo L., Renon N., Soulaine C., Hénon F., Tomer S.K., Labat D., Pokrovsky O.S., Sekhar M.,  Ababou R., Quintard M., 2014. An open source massively parallel solver for Richards equation: Mechanistic modeling of water fluxes at the watershed scale. Computer Physics Communications 185 : 3358-3371. DOI: 10.1016/j.cpc.2014.08.004
http://oatao.univ-toulouse.fr/12140/ 
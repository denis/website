Title: Numerical simulations of Quantum Turbulence in Supefluid Helium and Bose-Einstein condensates
Date: 2020-07-06 15:08
Slug: job_1414445ac8c9c9587573d763fbd2d522
Category: job
Authors: Ionut Danaila
Email: ionut.danaila@univ-rouen.fr
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: Rouen (St-Etienne-du-Rouvray)
Job_Duration: 12 mois
Job_Website: http://lmrs-num.math.cnrs.fr
Job_Employer: Université de Rouen
Expiration_Date: 2020-09-28
Attachment: job_1414445ac8c9c9587573d763fbd2d522_attachment.pdf

**Context:**

This study is part of a national (ANR) research program entitled QUTE-HPC (QUantum Turbulence Exploration by High-Performance Computing). The project gathers 10 researchers from physics and mathematics and is aimed at providing a new state-of-the-art for the mathematical-physical modelling and High Performance Computing (HPC) of Quantum Turbulence (QT) in superfluid quantum fluids, such as superfluid Helium (He) and atomic Bose-Einstein condensates (BEC). More information on the project can be find at http://qute-hpc.math.cnrs.fr/

**Research topic:**

One of the striking features of quantum fluids is the nucleation of vortices with quantized circulation, when an external forcing is applied (rotation, stirring, etc). Configurations with a large number of vortices tangled in space can evolve to Quantum Turbulence (QT). While QT in superfluid helium has been largely studied in the last two decades, only recent experimental and theoretical studies reported different possible routes to QT in Bose-Einstein Condensates (BEC).

The work will focus on the analysis, development and implementation of numerical schemes for high performance computations of QT.

* Main numerical simulations will be performed using the numerical code GPS, already developed in the framework of the project, using MPI and OpenMP programming. Different numerical schemes and discretizations (spectral, compact finite difference) are already available. First, the candidate will participate at consolidating the code with the objective to run massively parallel computations of QT based on the Gross-Pitaevskii equation. The spectral technical frame of the code will be then used to implement models for QT in superfluid helium: the two-fluid model, the HVBK model and new models developed in the project.
* As a companion of the HPC simulations, the development of a laboratory code based on finite elements and the software FreeFem++ will be pursued by the candidate. Domain decomposition numerical methods (DDM) and adaptive mesh refinement (AMR) algorithms are necessary to simulate 3D configurations with hundreds of vortices. This code will be specially designed for the simulation of QT in BEC.

**Requirements:**

The successful candidate is expected to hold (or about to have) a PhD in the area of computational physics or applied mathematics. Programming experience is essential. Experience in using high-performance computing facilities (HPC) and Freefem++ solver would be an advantage.
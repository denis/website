Title: CDI Ingénieur de Recherche en modélisation avancée et traitement de données LIDAR 3D
Date: 2020-06-10 12:51
Slug: job_2c30984c64f94aa2a4c3605db0750c69
Category: job
Authors: Nicolas Riviere
Email: Nicolas.Riviere@onera.fr
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: Toulouse
Job_Duration: 
Job_Website: 
Job_Employer: ONERA Toulouse
Expiration_Date: 2020-09-02
Attachment: job_2c30984c64f94aa2a4c3605db0750c69_attachment.pdf

**MISSIONS**

Vous intégrez l’unité de recherche « Interaction Onde-matière et systèmes laser pour la Détection directe et l&#39;Imagerie » (IODI) du Département Optique et Techniques Associées (DOTA) sur le site de Toulouse. Le LiDAR imageur 3D est au cœur des activités de recherche de l’Unité en alliant : la modélisation physique représentative (scène, atmosphère, capteur et traitement de signal), le développement de démonstrateurs instrumentaux pour évaluer de nouveaux concepts et les traitements de signaux associés. Nous mettons en œuvre des LiDAR 3D en exploitant l’information 3D radiométrique sur différentes plateformes mobiles (dont drones et avions) pour diverses applications comme la surveillance, la tenue de situation, le renseignement, l’aide à la navigation et la géomatique. Nous étudions de nouveaux concepts de LiDAR 3D embarquables et plus compacts. Devant la multiplicité et la complexité des architectures, l’étude de tels concepts passe aujourd’hui par un prototypage numérique en complément du développement de démonstrateurs instrumentaux.

Pour renforcer l&#39;équipe et préparer les ruptures de demain, vous participez, au sein de l’unité, au
développement et à la mise en œuvre des chaînes de modélisation et de traitement. En étroite
relation avec vos collègues, vous êtes force de proposition quant aux évolutions de celles-ci. Vous contribuez dans le cadre d’études aux maquettages numériques des prototypes incluant les algorithmes de traitement et prenez part à leurs validations à partir de données réelles. Vous mettez en œuvre sur nos démonstrateurs les chaînes de traitements de signaux (ex : encodage, filtrage, débruitage...) ou de données 3D (ex : détection changement, segmentation...) répondant aux besoins applicatifs et vous participez à l’exploitation des données 3D acquises lors d’essais.

Vos travaux s’inscrivent dans le cadre de projets pluridisciplinaires de recherche appliquée. Ils sont menés essentiellement en collaboration avec les spécialistes du Département. Vous êtes également amené(e) à travailler avec d’autres équipes de l’ONERA et des partenaires externes (services étatiques, industriels, laboratoires et établissements de recherche). Enfin, vous participez chaque fois que possible à la valorisation des travaux par des communications à congrès et des publications scientifiques. Vos missions sont conditionnées à l’obtention d’une habilitation de Défense Nationale.


**PROFIL**

Ingénieur(e) et/ou docteur(e) en Mathématiques Appliquées ayant des compétences en
optique/optronique ou ingénieur(e) et/ou docteur(e) en Physique doté(e) de compétences en traitement de signal/image. Une connaissance des systèmes LiDAR 3D et/ou de l’exploitation de
nuages de points 3D sont de réels atouts. Par ailleurs, vous disposez de compétences en
développement logiciel (C/C++, Python, CUDA...) et avez démontré par votre parcours un goût pour la modélisation physique. Motivé(e) par l’innovation et la technologie et muni(e) d’un bon relationnel, vous appréciez le travail collaboratif et le partage des connaissances. La maîtrise de l’anglais est indispensable.


Merci de déposer votre candidature (LM et CV) en ligne sur notre site www.onera.fr sous la
référence N° DDS/UNITE/CDI/000xxx
Title: Scientific Software Developer - Package Management
Date: 2021-04-03 11:56
Slug: job_3ddc50c26620d033ebf9cbb5d32f68dd
Category: job
Authors: Sylvain Corlay
Email: sylvain.corlay@quantstack.net
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: Remote
Job_Duration: 
Job_Website: https://quantstack.net
Job_Employer: QuantStack
Expiration_Date: 2021-06-26
Attachment: 

QuantStack is looking for a software developer to join our team of contributors to the conda and mamba ecosystems. In this role, you will become a member of a vibrant open-source community, contribute to a high-impact project used by millions in the world, and grow to become a key maintainer of the ecosystem.

**About QuantStack**

QuantStack is an open-source development studio specializing in scientific computing. The team comprises core developers of major open-source projects, such as Jupyter, Conda-forge, Voilà, Xtensor, Mamba, and many others. We strive to build high-impact software and grow communities.

**Your role at QuantStack**

You will support the community of users of the Conda and Mamba ecosystems, and help shape the future of software adopted by millions of engineers and scientists in the world. In this role, you will:

* Contribute to the development of the mamba package manager and related software
* Contribute to the mamba open-source project and work on features such as package signing, improved command-line interface, parallel package download and installation, and language bindings.
* Support the conda-forge community with the packaging of thousands of packages of the open-source scientific-computing ecosystem.

While prior experience with conda is not required, a track record of contributing to open-source software is a huge plus. For this role, we are seeking strong C++ and Python programmers, ideally experienced with build toolchains and best practices for package management on the main platforms.

**Where we can hire**

Our engineering team is fully remote. The company is headquartered in Paris, France, and the majority of our team is Europe-based. We will consider applications in the European Union.
Title: Data management for plankton images
Date: 2021-02-18 10:38
Slug: job_520a94780e1852f5e4593c0f7e4c9d35
Category: job
Authors: Jean-Olivier Irisson
Email: irisson@obs-vlfr.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Villefranche sur mer, France
Job_Duration: 8+11 mois
Job_Website: 
Job_Employer: Sorbonne Université
Expiration_Date: 2021-05-13
Attachment: job_520a94780e1852f5e4593c0f7e4c9d35_attachment.pdf

Manage the quantitative imaging data collected by the team and its partners through various ongoing projects.

Members of COMPLEx are involved in several ongoing projects: pan-oceanic cruises with the Tara shooner, TRIATLAS and AtlanECO for plankton ecology in the offshore Atlantic, JERICO-S3 for coastal observations, BlueCloud for data integration at European level, MarTERA and TechOceanS for instrument development, etc. In all projects, one of their goals is to collect plankton images and extract data from them. A major hindrance to 
the quick extraction of knowledge from the data is the amount of data handling needed between its acquisition and  its  use,  for  which  the  team  has  no  dedicated  expertise  and  therefore  developed  only  sub-optimal approaches. This position is destined to fulfill this pivotal role in the team’s activity.
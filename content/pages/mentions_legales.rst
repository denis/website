Mentions légales
################

:date: 2019-03-22
:modified: 2019-03-22
:slug: mentions-legales

.. contents::

.. section:: Rédaction et publication
    :class: description

    Directeur de la publication : M. Antoine Petit, Président-directeur général du CNRS

    Directeurs de la rédaction :

    - Matthieu Boileau
    - Anne Cadiou
    - Loïc Gouarin
    - Matthieu Haefele

    Rédacteurs en chef et relecteurs : `bureau du Groupe Calcul <{filename}presentation_groupe.rst#bureau>`_


.. section:: Hébergement et contact
    :class: description

    - Adresse de contact : `calcul-contact@math.cnrs.fr <calcul-contact@math.cnrs.fr>`_
    - Hébergement web : GDS Mathrice_
    - Hébergement des sources : `projet gitlab <https://gitlab.math.unistra.fr/groupe-calcul/website>`_ sur l'instance
      de l'IRMA_.


.. section:: Création du site
    :class: orga

    - Matthieu Boileau
    - Roland Denis
    - `Pierre Digonnet`_ (Graphisme)
    - Benoît Fabrèges
    - Loïc Gouarin
    - Matthieu Haefele
    - Pierre Navaro
    - Fabrice Roy


.. section:: Contenus
    :class: description

    Les sources de ce site web sont hébergées sur ce
    `projet gitlab <https://gitlab.math.unistra.fr/groupe-calcul/website>`_.

    Sauf mention contraire, la réutilisation des contenus de calcul.math.cnrs.fr est soumise aux règles précisées
    ci-dessous.

    - Textes

      Le texte des articles est mis à disposition selon les termes de licence
      `CC-BY-4.0-FR`_ à la condition suivante : vous devez
      créditer les contenus, intégrer un lien vers la licence et préciser la date à
      laquelle le contenu a été extrait du site. Vous devez indiquer ces informations par tous les moyens
      raisonnables, sans toutefois suggérer que le Groupe Calcul vous soutient ou soutient la façon dont vous avez
      utilisé ses contenus.

    - Logo

      Le `logo <{filename}logo.rst>`_ du Groupe Calcul est distribué sous licence `CC-BY-NC-ND-2.0-FR`_.

    - Autres images, vidéos et iconographies

      Les images disponibles sur le site comprennent les photographies, captures écran et illustrations.
      Sous réserve des droits de propriété intellectuelle de tiers, les images qui sont la propriété exclusive du
      Groupe Calcul sont, sauf mention contraire, mises à disposition selon les termes de la licence
      `CC-BY-4.0-FR`_ à la condition suivante : vous devez créditer les contenus, intégrer un lien vers la licence et
      préciser la date à laquelle le contenu a été extrait du site. Vous devez indiquer ces informations par tous les
      moyens raisonnables, sans toutefois suggérer que le Groupe Calcul vous soutient ou soutient la façon dont vous
      avez utilisé ses contenus.

      Les pictogrammes proviennent des sources suivantes :

        - `Pawel Rak <https://thenounproject.com/pr3113738/>`_ , `Adi Kurniawan <https://thenounproject.com/kuradn/>`_ , `Becris <https://thenounproject.com/becris/>`_ , `Edwin PM <https://thenounproject.com/edwin.misran/>`_ , `Genius Icons <https://thenounproject.com/rajappar29/>`_ et `Template <https://thenounproject.com/paisurangkana>`_ sur Noun Project,
        - Site `iconfinder <https://www.iconfinder.com>`_,
        - `Pierre Digonnet`_ (licence `CC-BY-4.0-FR`_).


    - Supports et résumés de présentations

      Le contenu est soumis aux termes de la licence indiquée par l'auteur du support.
      En l'absence de licence, le contenu ne peut pas être réutilisé sans l'accord de  l'auteur.


    - Données ouvertes

      Les données publiques détenues ou produites par le Groupe Calcul dans le cadre de l'open data sont mises à
      disposition par défaut selon les termes de la
      `licence ouverte <https://www.etalab.gouv.fr/wp-content/uploads/2014/05/Licence_Ouverte.pdf>`_.


.. section:: Données
    :class: description

    Chaque service en ligne, en particulier le portail `Indico <https://indico.mathrice.fr/category/60>`_ de Mathrice,
    limite la collecte des données personnelles au strict nécessaire et s’accompagne d’une information sur :

    - les objectifs du recueil de ces données (finalités) ;
    - la base juridique du traitement de données ;
    - le caractère obligatoire ou facultatif du recueil des données pour la gestion de votre demande et le rappel des
      catégories de données traitées ;
    - les catégories de personnes concernées ;
    - les destinataires des données ;
    - la durée de conservation des données ;
    - les mesures de sécurité (description générale) ;
    - vos droits Informatique et Libertés et la façon de les exercer auprès du Groupe Calcul.

    Les données personnelles recueillies dans le cadre des services proposés sur calcul.math.cnrs.fr sont traitées selon
    des protocoles sécurisés.


    .. section:: Formulaires d'inscription sur Indico
        :class: description

        Quatre catégories de personnes sont concernées par ces données :

        - les participants,
        - les organisateurs,
        - les intervenants,
        - les prestataires missionnés par les organisateurs.

        Les destinataires des données sont principalement les organisateurs, éventuellement les intervenants (sondage
        pré-formation, par exemple) et au besoin les prestataires (dans le cas d'un hébergement hôtelier, par exemple).
        Les données seront conservées sur le serveur Indico qui les héberge tant que les personnes concernées n'en
        demandent pas la suppresssion.
        L'accès aux données se fait depuis les comptes d'administration de l'évènement Indico via le protocole https et
        après authentification par mot de passe.

    .. section:: Formulaire de publication d'une offre d'emploi
        :class: description

        Les personnes concernées par ces données sont les auteurs des dépôts d'annonces sur le `formulaire <../job_offers/add_job_offer>`_.
        Les données collectées sont utilisées pour :

        - publier l'annonce d'offre d'emploi sur la `page dédiée <../category/job.html>`_,
        - annoncer cette publication sur la liste de diffusion `calcul@listes.math.cnrs.fr <#liste-de-diffusion-calcullistesmathcnrsfr>`_.
        - annoncer cette publication sur le `fil twitter <https://twitter.com/GroupeCalcul>`_ du groupe Calcul.

        Les destinataires des données sont principalement les membres de la communauté du calcul.
        L'auteur du dépôt d'annonce est invité à préciser une date d'expiration pour cette annonce.
        Au-delà de cette date, l'annonce et son éventuelle pièce jointe seront automatiquement supprimées du site.
        L'auteur peut également demander la suppression de l'annonce en écrivant à
        `calcul-contact@math.cnrs.fr <calcul-contact@math.cnrs.fr>`_.

    .. section:: Liste de diffusion calcul@listes.math.cnrs.fr
        :class: description

        Les personnes concernées par ces données sont les abonnés de la liste de diffusion.
        Cette liste est gérée par le serveur de listes `Sympa <https://listes.math.cnrs.fr>`_ qui est administré par
        `Mathrice <https://www.mathrice.fr>`_.
        Les données concernent :

        - les noms des abonnés,
        - les adresses de messagerie des abonnés,
        - le contenu des messages échangés par les abonnés.

        Ces données sont utilisées pour échanger des informations (offres d'emploi, annonce d'évènements, questions
        techniques, etc.) au sein de la communauté du calcul scientifique.
        Les modérateurs de la liste font partie du `bureau <presentation_groupe.html#bureau>`_ du groupe Calcul.

        Chaque membre peut demander à être désabonné de la liste.
        Les messages échangés sont stockés dans les archives du serveur sympa qui sont consultables en ligne par les
        abonnés de la liste.
        Ces archives sont munis d'un moteur de recherche qui permet de retrouver les discussions sur des points
        techniques ou scientifique spécifiques qui intéressent la communauté du calcul.


.. section:: Vos droits
    :class: description

    Pour toute information ou exercice de vos droits Informatique et Libertés sur les traitements de données
    personnelles gérés par le Groupe Calcul, vous pouvez contacter le bureau du Groupe Calcul à
    `calcul-contact@math.cnrs.fr <calcul-contact@math.cnrs.fr>`_ ou bien la déléguée à la protection des données
    (DPD) du CNRS `Gaëlle Bujan <http://www.cnrs.fr/fr/personne/gaelle-bujan>`_ aux coordonnées suivantes :

    - Courriel : `dpd@cnrs.fr <dpd@cnrs.fr>`_
    - Téléphone : 03 83 85 64 26
